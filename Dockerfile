FROM node:10-alpine

RUN mkdir -p /home/node/app/node_modules

WORKDIR /home/node/app
COPY package.json ./
COPY webpack.config.js ./
COPY app/ ./app/
COPY src/ ./src/
RUN npm install && NODE_ENV=production npm run build

EXPOSE 3000
CMD [ "npm", "start" ]
