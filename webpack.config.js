const path = require('path')
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
    mode: process.env.NODE_ENV || 'development',
    entry: './src/web/js/index.js',
    output: {
        path: path.resolve(__dirname, 'app/static'),
        filename: 'js/bundle.js'
    },
    plugins: [
        new CopyWebpackPlugin(
            [
                {
                    from: path.resolve(__dirname, 'src/web'),
                    to: path.resolve(__dirname, 'app/static'),
                    ignore: [
                        '!*.html',
                        'node_modules/*'
                    ]
                },
            ],
            { context: path.resolve(__dirname, 'src/web') }
        ),
        new CopyWebpackPlugin(
            [
                {
                    from: path.resolve(__dirname, 'src/web/images'),
                    to: path.resolve(__dirname, 'app/static/images/'),
                    ignore: [
                        'node_modules/*'
                    ]
                },
            ],
            { context: path.resolve(__dirname, 'src/web/images') }
        ),
        new WriteFilePlugin(),
        new VueLoaderPlugin(),
        new HardSourceWebpackPlugin(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: ['vue-style-loader', 'css-loader']
            },
            {
                test: /\.s(c|a)ss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                            fiber: require('fibers'),
                            indentedSyntax: true
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
        },
    },
    externals: {
        'react-native-sqlite-storage': 'react-native-sqlite-storage'
    }
}