var CHARACTORS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
var CHARACTORS_LENGTH = CHARACTORS.length;
const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY || "KneD9DnackD8dnJE93Kd9gNgge38JgKi" // 32Byte
const BUFFER_KEY = process.env.BUFFER_KEY || "Dng9dE93NgG13Dlg" // 16Byte
const ENCRYPT_METHOD = "aes-256-cbc"
const ENCODING = "base64"
let IV = Buffer.from(BUFFER_KEY)
const crypto = require('crypto');

module.exports = {
    isUrl(arg) {
        return arg && arg.match(/^(https?)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/);
    },
    isInteger(arg) {
        try {
            var parsed = parseInt(arg);
            return !!(parsed == 0 ? true : parsed);
        } catch (ignore) {
            return false;
        }
    },
    isNumberFormat(arg) {
        return `${arg}`.match(/^\d+$/);
    },
    isDateFormat(arg) {
        return arg && arg.match(/^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])$/);
    },
    isMail(arg) {
        if (!arg) {
            return false;
        }
        return arg.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/);
    },
    randomString(length) {
        var result = [];
        for (var i = 0; i < length; i++) {
            result.push(CHARACTORS.charAt(Math.floor(Math.random() * CHARACTORS_LENGTH)));
        }
        return result.join("");
    },
    fixedEncodeURIComponent(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    },
    encrypt(raw) {
        let cipher = crypto.createCipheriv(ENCRYPT_METHOD, Buffer.from(ENCRYPTION_KEY), IV)
        let encrypted = cipher.update(raw)
        encrypted = Buffer.concat([encrypted, cipher.final()])
        return encrypted.toString(ENCODING)
    },
    decrypt(encrypted) {
        let encryptedText = Buffer.from(encrypted, ENCODING)
        let decipher = crypto.createDecipheriv(ENCRYPT_METHOD, Buffer.from(ENCRYPTION_KEY), IV)
        let decrypted = decipher.update(encryptedText)
        decrypted = Buffer.concat([decrypted, decipher.final()])
        return decrypted.toString()
    },
    epocToDateFormat(epoc) {
        if (!epoc) {
            return null;
        }
        const date = new Date(epoc);
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    },
    dateFromatToEpoc(dateFormat) {
        if (!dateFormat) {
            return null;
        }
        try {
            const date = new Date(dateFormat + " 00:00:00");
            return date.getTime();
        } catch (e) {
            return null;
        }
    },
    toDate(src) {
        if (!src) {
            return src;
        }
        try {
            return new Date(src);
        } catch (e) {
            return null;
        }
    },
    toISOString(src) {
        const date = this.toDate(src);
        if (!date || date.toString() === "Invalid Date") {
            return null;
        }
        try {
            return date.toISOString();
        } catch (ignore) {
            return null;
        }
    },
    toInputDateString(src) {
        const date = this.toDate(src);
        if (!date || date.toString() === "Invalid Date") {
            return null;
        }
        try {
            return date.getFullYear() + "-" + this.zeroPad(date.getMonth() + 1, 2) + "-" + this.zeroPad(date.getDate(), 2);
        } catch (ignore) {
            console.log(ignore + ":" + ignore.stack);
            return null;
        }
    },
    zeroPad(src, length) {
        return ("000000000" + src).slice(-1 * length);
    }
}