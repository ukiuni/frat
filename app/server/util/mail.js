var mailer = require('nodemailer');

var setting = {
    host: process.env.MAIL_HOST || 'localhost',
    port: process.env.MAIL_PORT || '1025',
    secure: false,
    auth: {
        user: process.env.MAIL_USRE || 'user',
        pass: process.env.MAIL_PASS || 'secret'
    }
};

var mailOptions = {
    from: process.env.MAIL_FROM || 'fRat@example.com',
    to: '送信先メールアドレス',
    subject: 'メールの件名',
    text: 'メールの内容',
    html: '<html>メールの内容</html>'
};

module.exports = async (mail) => {
    mail.from = mailOptions.from;
    return new Promise((resolve, reject) => {
        var smtp = mailer.createTransport(setting);
        smtp.sendMail(mail, function (err, res) {
            if (err) {
                reject(err);
            } else {
                resolve(res.message);
            }
            smtp.close();
        });
    });
}