const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "message_link";
class MessageLink {
    constructor(deafultArgs) {
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    MessageLink,
    table,
    targetName: "MessageLink"
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: todoTargetName } = require("../model/Todo");
const { targetName: tenantTargetName } = require("./Tenant");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: MessageLink,
    columns: {
        comment: {
            type: "varchar"
        },
        url: {
            type: "varchar"
        },
        todoId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false
        },
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true,
            inverseSide: 'messageLinks'
        },
    },
    indices: [{ unique: false, columns: ["todo"] }]
}, DefaultEntity));