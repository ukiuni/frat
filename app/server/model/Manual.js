const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "manual";
class Manual {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.title = null;
        this.description = null;
        this.content = null;
        this.url = null;
        this.shareStartAt = null;
        this.shareEndAt = null;
        this.groupId = null;
        this.register = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    Manual,
    table,
    targetName: "Manual"
}

const { targetName: accountTargetName } = require("./Account")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Manual,
    columns: {
        title: {
            type: "varchar"
        },
        description: {
            type: "varchar",
            nullable: true
        },
        content: {
            type: "varchar"
        },
        url: {
            type: "varchar",
            nullable: true
        },
        shareStartAt: {
            type: "timestamp",
            nullable: true
        },
        shareEndAt: {
            type: "timestamp",
            nullable: true
        },
        groupId: {
            type: "int"
        }
    },
    relations: {
        register: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        }
    }
}, DefaultEntity));