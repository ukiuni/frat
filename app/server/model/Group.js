const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const db = require("../util/db")
const table = "group";
const Strings = require("../util/stringUtil");
class Group {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.name = null;
        this.key = null;
        this.description = null;
        this.iconUrl = null;
        this.members = null;
        Object.assign(this, deafultArgs);
    }
}
const forExport = {
    Group,
    table,
    targetName: "Group",
    accountRelationType: {
        MEMBER: "MEMBER",
        VIEWER: "VIEWER"
    }
}
module.exports = forExport;
const { targetName: accountTargetName, table: accountTable } = require("./Account")
const { targetName: todoTargetName } = require("./Todo")
const { targetName: tenantTargetName } = require("./Tenant")
const { targetName: customAttributeTargetName, table: customAttributeTable } = require("./CustomAttribute")
const { targetName: fileTargetName } = require("./File")
const { targetName: todoTemplateTargetName } = require("./TodoTemplate")
const { targetName: integrationTargetName } = require("./Integration")
Group.isMember = async (tenant, groupId, account, srcGroup) => {
    const group = srcGroup || await Group.findGroupWithMember(tenant, groupId);
    return (
        group
        && (
            (group.members && group.members.some(t => t.id == account.id))
            || (group.viewMembers && group.viewMembers.some(t => t.id == account.id))
        )
    );
}
Group.openGroupInTenant = async (tenant) => {
    return await db.createQueryBuilder(table)
        .where(`"tenantId"=:tenantId and ("${table}".secret = false or "${table}".secret IS NULL) and "${table}"."deletedAt" IS NULL`, { tenantId: tenant.id })
        .getMany();
}
Group.findGroupWithMember = async (tenant, groupIdOrKey) => {
    var idisIdNotKey = Strings.isNumberFormat(groupIdOrKey);
    var idQuery = idisIdNotKey ? `${table}.id = :id` : `${table}.key = :id`
    var group = await db.createQueryBuilder(table)
        .leftJoinAndSelect(table + ".todoTemplate", todoTemplateTargetName, `"${todoTemplateTargetName}"."deletedAt" IS NULL`)
        .leftJoinAndSelect(table + ".integrations", "integrations", `integrations."deletedAt" IS NULL`)
        .leftJoinAndSelect(table + ".customAttributes", customAttributeTable, `"${customAttributeTable}"."deletedAt" IS NULL`)
        .where(idQuery + ` and "${table}"."tenantId"=:tenantId and "${table}"."deletedAt" IS NULL`, { id: groupIdOrKey, tenantId: tenant.id })
        .getOne();
    if (!group) {
        return group;
    }
    var groupMember = (await db.createQueryBuilder(table)
        .leftJoinAndSelect(table + ".members", accountTable, `"${accountTable}"."deletedAt" IS NULL`)
        .orderBy(`"${accountTable}".id`)
        .where(idQuery + ` and "${table}"."tenantId"=:tenantId and "${table}"."deletedAt" IS NULL`, { id: groupIdOrKey, tenantId: tenant.id })
        .getOne()).members;
    var groupViewMember = (await db.createQueryBuilder(table)
        .leftJoinAndSelect(table + ".viewMembers", accountTable, `"${accountTable}"."deletedAt" IS NULL`)
        .orderBy(`"${accountTable}".id`)
        .where(idQuery + ` and "${table}"."tenantId"=:tenantId and "${table}"."deletedAt" IS NULL`, { id: groupIdOrKey, tenantId: tenant.id })
        .getOne()).viewMembers;

    group.members = groupMember.map(a => { return { id: a.id, name: a.name, type: forExport.accountRelationType.MEMBER, iconUrl: a.iconUrl } });
    group.viewMembers = groupViewMember.map(a => { return { id: a.id, name: a.name, type: forExport.accountRelationType.VIEWER, iconUrl: a.iconUrl } });
    return group;
}
const replaced = "oabcdefghi"
const intToChar = (intValue) => {
    return `${intValue}`.replace(/\d/g, (i) => replaced[i])
}
Group.createTodoSequence = async (tenant, group, sequenceStart) => {
    return await db.createSequence("todoseqof" + intToChar(tenant.id) + "z" + intToChar(group.id), sequenceStart);
}
Group.nextTodoSequenceValue = async (tenant, group) => {
    return await db.nextSequenceValue("todoseqof" + intToChar(tenant.id) + "z" + intToChar(group.id));
}

module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Group,
    columns: {
        secret: {
            type: "boolean",
            nullable: true
        },
        name: {
            type: "varchar"
        },
        key: {
            type: "varchar"
        },
        description: {
            type: "varchar",
            nullable: true
        },
        iconUrl: {
            type: "varchar",
            nullable: true
        }
    },
    relations: {
        members: {
            target: accountTargetName,
            type: "many-to-many",
            cascade: false,
            joinTable: {
                name: 'group_account',
                joinColumn: {
                    name: 'groupId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'groups',
            }
        },
        viewMembers: {
            target: accountTargetName,
            type: "many-to-many",
            cascade: false,
            joinTable: {
                name: 'view_group_account',
                joinColumn: {
                    name: 'groupId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'viewGroups',
            }
        },
        todos: {
            target: todoTargetName,
            type: "one-to-many",
            cascade: false
        },
        customAttributes: {
            target: customAttributeTargetName,
            type: "one-to-many",
            cascade: false,
            inverseSide: "group",
        },
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'groups'
        },
        files: {
            target: fileTargetName,
            type: "one-to-many",
            cascade: false,
            inverseSide: "group",
        },
        todoTemplate: {
            target: todoTemplateTargetName,
            type: "one-to-one",
            cascade: false,
            inverseSide: "group",
        },
        integrations: {
            target: integrationTargetName,
            type: "one-to-many",
            cascade: false,
            inverseSide: "group",
        }
    },
    indices: [{ unique: true, columns: ["tenant", "key"] }]
}, DefaultEntity));