module.exports = {
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        createdAt: {
            type: "timestamp"
        },
        createdAccountId: {
            type: "int",
            nullable: true
        },
        updatedAt: {
            type: "timestamp",
            nullable: true
        },
        updatedAccountId: {
            type: "int",
            nullable: true
        },
        deletedAt: {
            type: "timestamp",
            nullable: true
        },
        deletedAccountId: {
            type: "int",
            nullable: true
        }
    }
}