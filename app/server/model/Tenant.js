const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");

const table = "tenant";
class Tenant {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.key = 0;
        this.name = 0;
        Object.assign(this, deafultArgs);
    }
}
Tenant.DEFAULT_FILE_SIZE_LIMIT = 1000000000;
Tenant.DEFAULT_FILE_SIZE_LIMIT_PER_ACCOUNT = 1000000000;
Tenant.DEFAULT_GROUP_COUNT_LIMIT = 2;
Tenant.DEFAULT_ACCOUNT_COUNT_LIMIT = 5;
module.exports = {
    Tenant,
    table,
    targetName: "Tenant",
    defaultDomains: (process.env.DEFAULT_DOMAINS || "localhost").split(","),
}

const { targetName: accountTargetName } = require("./Account")
const { targetName: groupTargetName } = require("./Group")
const { targetName: todoTargetName } = require("./Todo")
const { targetName: accountTenantRelationTargetName } = require("./AccountTenantRelation")
const { table: tableAccount } = require("../model/Account");
const { table: tableAccountTenantRelation } = require("../model/AccountTenantRelation");
const db = require("../util/db");
Tenant.loadRelation = async (tenant) => {
    return await db.createQueryBuilder(tableAccountTenantRelation).leftJoinAndSelect(tableAccountTenantRelation + ".account", tableAccount)
        .where(`"${tableAccountTenantRelation}"."tenantId" = :tenantId and "${tableAccountTenantRelation}"."deletedAt" IS NULL`, { tenantId: tenant.id })
        .orderBy(`"${tableAccountTenantRelation}"."createdAt"`)
        .getMany();
}
Tenant.loadFileInfo = async (tenant) => {
    const info = await db.getConnection().query(`select count(1) as count, sum(size) as sum from file where "tenantId"=$1 and file."deletedAt" IS NULL`, [tenant.id]);
    return info[0];
}
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Tenant,
    columns: {
        key: {
            type: "varchar",
            unique: true
        },
        name: {
            type: "varchar",
            nullable: true
        },
        fileSizeLimit: {
            type: "int",
            nullable: true
        },
        accountCountLimit: {
            type: "int",
            nullable: true
        },
        groupCountLimit: {
            type: "int",
            nullable: true
        },
        accessibleIpRanges: {
            type: "varchar",
            nullable: true
        }
    },
    relations: {
        accounts: {
            target: accountTargetName,
            type: "many-to-many",
            joinTable: {
                name: 'tenant_account',
                joinColumn: {
                    name: 'tenantId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'tenants',
            },
            cascade: false,
            inverseSide: "tenant"
        },
        accountRelations: {
            target: accountTenantRelationTargetName,
            type: "one-to-many",
            joinTable: true,
            inverseSide: 'tenant',
        },
        groups: {
            target: groupTargetName,
            type: "one-to-many",
            joinTable: true,
            cascade: false,
            inverseSide: "tenant"
        },
        todos: {
            target: todoTargetName,
            type: "one-to-many",
            joinTable: true,
            cascade: false,
            inverseSide: "tenant"
        }
    }
}, DefaultEntity));