const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "todo_todo_relation";
class TodoTodoRelation {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.relationTodoId = null;
        this.relatedTodoId = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    TodoTodoRelation,
    table,
    targetName: "TodoTodoRelation"
}

const { targetName: todoTargetName } = require("./Todo");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: TodoTodoRelation,
    columns: {
        relationTodoId: {
            type: "int"
        },
        relatedTodoId: {
            type: "int"
        },
        order: {
            type: "int"
        }
    },
    relations: {
        relationTodo: {
            target: todoTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        relatedTodo: {
            target: todoTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        }
    }
}, DefaultEntity));