const EntitySchema = require("typeorm").EntitySchema;
const diff = require('deep-diff').diff;
const Diff = require('diff');
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const aIsLargerThanB = require("../util/calcOrderKey").aIsLargerThanB;
const table = "event";
const sortCustomAttributeValues = (customAttributeValues) => {
    return customAttributeValues.sort((c1, c2) => c2.customAttribute.order - c1.customAttribute.order)
}
class Event {
    constructor(defaultArg) {
        //fields
        if (!defaultArg) {
            return;
        }

        if (defaultArg.before) {
            if (defaultArg.before.customAttributeValues) {
                defaultArg.before.customAttributeValues = sortCustomAttributeValues(defaultArg.before.customAttributeValues)
            }
            if (defaultArg.source.customAttributeValues) {
                defaultArg.source.customAttributeValues = sortCustomAttributeValues(defaultArg.source.customAttributeValues)
            }
        }
        Object.assign(this, defaultArg);
        defaultArg.diff = diff(defaultArg.before, defaultArg.source)
        if (!defaultArg.summary) {
            this.summary = Event.createSummary(defaultArg);
        }
        this.diff = JSON.stringify(defaultArg.diff)
        this.source = JSON.stringify(defaultArg.source)
    }
}
module.exports = {
    Event,
    table,
    targetName: "Event",
    type: {
        TODO_CREATE: "TODO_CREATE",
        TODO_UPDATE: "TODO_UPDATE",
        TODO_DELETE: "TODO_DELETE",
        TODO_UPDATE_PROGRESS: "TODO_UPDATE_PROGRESS",
        TODO_CHANGE_ORDER: "TODO_CHANGE_ORDER",
        SPRINT_CREATE: "SPRINT_CREATE",
        SPRINT_CHANGE_ORDER: "SPRINT_CHANGE_ORDER",
        WIKI_CREATE: "WIKI_CREATE",
        WIKI_UPDATE: "WIKI_UPDATE"
    }
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: todoTargetName } = require("../model/Todo");
const { targetName: tenantTargetName } = require("./Tenant");
const { targetName: wikiTargetName } = require("./Wiki");
const { targetName: integrationTargetName } = require("./Integration");
const { targetName: sprintBarTargetName } = require("./SprintBar");
const { type: customAttributeValueType } = require("../model/CustomAttribute");
const customAttribyteValueTypeArray = ["stringValue", "intValue", "dateValue"];
const ifItemHasOneValue = diff => {
    return diff.item &&
        (
            (diff.path[0] == "customAttributeValues" &&
                (diff.item.lhs && customAttribyteValueTypeArray.find(k => diff.item.lhs[k])
                    || diff.item.rhs && customAttribyteValueTypeArray.find(k => diff.item.rhs[k]))
            )
            || diff.path[0] != "customAttributeValues"
        )

}
Event.createSummary = ({ type, diff, source }) => {
    var message = { type };
    if (type == "TODO_CHANGE_ORDER") {
        const progressDiff = diff.find(diff => diff.path && diff.path[0] == "orderKey")
        if (progressDiff) {
            message.changed = aIsLargerThanB(progressDiff.lhs, progressDiff.rhs) ? "higher" : "lower";//less number is higher.
        } else {
            message.changed = "higher"; //hmm. how i should do with incase new order?
        }
        return message;
    }
    if (type == "SPRINT_CHANGE_ORDER") {
        if (diff[0] && diff[0].lhs) {
            message.changed = aIsLargerThanB(diff[0].lhs, diff[0].rhs) ? "higher" : "lower";//less number is higher.
        } else {
            message.changed = "higher"; //hmm. how i should do with incase new order?
        }
        return message;
    }
    if (!diff || diff.length == 0 || !source) {
        return message;
    }
    if (type == "WIKI_UPDATE") {
        message.changed = diff.filter(d => d && (d.lhs || d.rhs) && d.path && (d.path[0] == "title" || d.path[0] == "content"))
            .map(d => {
                if (d.path[0] == "title") {
                    return {
                        title: d.path[0],
                        from: d.lhs,
                        to: d.rhs
                    }
                } else {//content
                    return {
                        title: d.path[0],
                        diff: Diff.diffWords(d.lhs || "", d.rhs || "")
                    }
                }
            })
        return message;
    }

    if (type != "TODO_UPDATE") {
        return message;
    }

    message.changed = diff.filter(
        diff => diff && (diff.lhs || diff.rhs || ifItemHasOneValue(diff)) && diff.path && diff.path[0] != "updatedAt" && diff.path[0] != "order" && diff.path[2] != "updatedAt" && diff.path[2] != "createdAt" && diff.path[2] != "customAttribute" && !(diff.path[0] == "customAttributeValues" && diff.path[2] == "id") && !(diff.path[0] == "parent" && !diff.item && diff.path[1] && diff.path[1] != "key") && !((diff.path[0] == "children" || diff.path[0] == "relations") && !diff.item && diff.path[2] != "key")
    ).map(diff => {
        if (diff.path[0] == "customAttributeValues") {
            var cav, lhs, rhs;
            if (diff.item) {//new
                cav = source[diff.path[0]][diff.index];
                const diffData = customAttribyteValueTypeArray.map(k => {
                    return {
                        lhs: diff.item.lhs ? diff.item.lhs[k] : null,
                        rhs: diff.item.rhs ? diff.item.rhs[k] : null,
                    }
                }).find(l_r => l_r.lhs || l_r.rhs);
                lhs = diffData.lhs;
                rhs = diffData.rhs;
            } else {//ovewrite
                cav = source[diff.path[0]][diff.path[1]];
                lhs = diff.lhs;
                rhs = diff.rhs;
            }
            if (cav.customAttribute.type == customAttributeValueType.TEXTAREA) {
                return {
                    title: cav.customAttribute.title,
                    diff: Diff.diffWords(lhs || "", rhs || ""),
                    customAttribute: true
                }
            }
            return {
                title: cav.customAttribute.title,
                from: lhs,
                to: rhs,
                customAttribute: true
            };
        } else if (diff.path[0] == "content") {
            return {
                title: diff.path[0],
                diff: Diff.diffWords(diff.lhs || "", diff.rhs || "")
            }
        } else if (diff.path[0] == "personnels" || diff.path[0] == "files") {
            if (!diff.item) {//no contents. case just id changed.
                return {
                    title: diff.path[0],
                    from: diff.lhs,
                    to: diff.rhs
                }
            }
            return {
                title: diff.path[0],
                from: diff.item.lhs && diff.item.lhs.name,
                to: diff.item.rhs && diff.item.rhs.name,
                addOrRemove: true
            }
        } else if (diff.path[0] == "children" || diff.path[0] == "relations") {
            if (!diff.item) {//appends
                return {
                    title: diff.path[0],
                    from: diff.lhs,
                    to: diff.rhs
                }
            }
            return {
                title: diff.path[0],
                from: diff.item.lhs && diff.item.lhs.key,
                to: diff.item.rhs && diff.item.rhs.key,
                addOrRemove: true
            }
        } else if (diff.path[0] == "parent") {
            if (!diff.path[1]) {//append parent
                return {
                    title: diff.path[0],
                    from: diff.lhs && diff.lhs.key,
                    to: diff.rhs && diff.rhs.key
                }
            }
            return {
                title: diff.path[0],
                from: diff.lhs,
                to: diff.rhs
            }
        } else {
            return {
                title: diff.path[0],
                from: diff.lhs,
                to: diff.rhs
            }
        }
    })
    return message;
}
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Event,
    columns: {
        type: {
            type: "varchar"
        },
        comment: {
            type: "varchar",
            nullable: true
        },
        diff: {
            type: "varchar",
            nullable: true
        },
        summary: {
            type: "json",
            nullable: true
        },
        source: {
            type: "varchar",
            nullable: true
        },
        ownerId: {
            type: "int",
            nullable: true
        },
        groupId: {
            type: "int",
            nullable: true
        },
        todoId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'groups'
        },
        owner: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false,
            nullable: true
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true,
        },
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true
        },
        wiki: {
            target: wikiTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true
        },
        integration: {
            target: integrationTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true
        },
        sprintBar: {
            target: sprintBarTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true
        }
    }
}, DefaultEntity));