const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "chat_message";
class ChatMessage {
    constructor(deafultArgs) {
        //fields
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    ChatMessage,
    table,
    targetName: "ChatMessage"
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: todoTargetName } = require("../model/Todo");
const { targetName: tenantTargetName } = require("./Tenant");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: ChatMessage,
    columns: {
        content: {
            type: "varchar"
        },
        ownerId: {
            type: "int"
        },
        groupId: {
            type: "int",
            nullable: true
        },
        todoId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'groups'
        },
        owner: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true,
        },
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            cascade: false,
            nullable: true,
            inverseSide: 'events'
        },
    }
}, DefaultEntity));