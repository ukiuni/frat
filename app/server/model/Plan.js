const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const db = require("../util/db")
const table = "plan";
class Plan {
    constructor(deafultArgs) {
        //fields
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    Plan,
    table,
    targetName: "plan"
}

const { targetName: accountTargetName } = require("./Account");
const { Tenant, targetName: tenantTargetName } = require("./Tenant");

Plan.loadPlan = async (tenant) => {
    return await db.findOne(tenant, table, '"deletedAt" IS NULL');
}
Plan.loadAllPlan = async () => {
    return await db.find(null, table, '"deletedAt" IS NULL');
}

Plan.getFileSizeLimit = (plan) => {
    if (plan == null) {
        return Tenant.DEFAULT_FILE_SIZE_LIMIT;
    }
    const planId = plan.plan;
    if (null == planId) {
        return Tenant.DEFAULT_FILE_SIZE_LIMIT;
    } else if ("free" == planId) {
        return Tenant.DEFAULT_FILE_SIZE_LIMIT;
    } else if ("standard" == planId) {
        return 30000000000;
    } else if ("premium" == planId) {
        return 100000000000;
    }
}
Plan.getGroupCountLimit = (plan) => {
    if (plan == null) {
        return Tenant.DEFAULT_GROUP_COUNT_LIMIT;
    }
    const planId = plan.plan;
    if (null == planId) {
        return Tenant.DEFAULT_GROUP_COUNT_LIMIT;
    } else if ("free" == planId) {
        return Tenant.DEFAULT_GROUP_COUNT_LIMIT;
    } else if ("standard" == planId) {
        return 10;
    } else if ("premium" == planId) {
        return Number.MAX_SAFE_INTEGER;
    }
}
Plan.getAccountCountLimit = (plan) => {
    if (plan == null) {
        return Tenant.DEFAULT_ACCOUNT_COUNT_LIMIT;
    }
    const planId = plan.plan;
    if (null == planId) {
        return Tenant.DEFAULT_ACCOUNT_COUNT_LIMIT;
    } else if ("free" == planId) {
        return Tenant.DEFAULT_ACCOUNT_COUNT_LIMIT;
    } else if ("standard" == planId) {
        return 100;
    } else if ("premium" == planId) {
        return 200;
    }
}
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Plan,
    columns: {
        plan: {
            type: "varchar"
        },
        cardTokenId: {
            type: "varchar",
            nullable: true
        },
        tenantId: {
            type: "int"
        },
        payerId: {
            type: "int"
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'groups'
        },
        payer: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        }
    }
}, DefaultEntity));