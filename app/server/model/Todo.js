const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "todo";
class Todo {
    constructor(deafultArgs) {
        //fields
        this.title = null;
        this.content = null;
        this.personnels = null;
        this.from = null;
        this.limitedAt = null;
        this.personnel = null;
        this.from = null;
        this.progress = null;
        this.parent = null;
        this.children = null;
        this.key = null;
        this.point = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    Todo,
    table,
    targetName: "Todo",
    type: {
        MEMBER_TODO_PARENT: "member_todo_parent",
        MEMBER_TODO_CHILD: "member_todo_child",
        CHILD: "child"
    },
    childProgressAvr(todo) {
        var childProgressAvr =
            todo.children
                .map(c => c.progress || 0)
                .reduce((sum, cur) => sum + cur) / todo.children.length;
        return Math.ceil(childProgressAvr);
    }
}

const { targetName: accountTargetName, table: accountTable } = require("../model/Account");
const { table: tableGroup, targetName: groupTargetName } = require("../model/Group");
const { targetName: todoOrderTargetName, table: todoOrderTable, type: TodoOrderType } = require("../model/TodoOrder");
const { targetName: tenantTargetName } = require("./Tenant")
const { targetName: fileTargetName, table: tableFile } = require("./File")
const { table: tableCustomAttribute } = require("../model/CustomAttribute");
const { targetName: customAttributeValueTargetName, table: tableCustomAttributeValue } = require("../model/CustomAttributeValue");
const { targetName: messageLinkargetName } = require("../model/MessageLink");
const db = require("../util/db");
Todo.loadTodo = async (tenant, group, additinalWhere, limit) => {
    var additionalWehreSQL = additinalWhere ? " " + additinalWhere.sql : "";
    const sqlWhere = `${table}."tenantId"=:tenantId ${group ? 'and "' + table + '"."groupId" = :groupId' : ""} and ${table}."deletedAt" IS NULL ` + additionalWehreSQL;
    var where = { tenantId: tenant.id };
    if (group) {
        where.groupId = group.id;
    }
    if (additinalWhere) {
        where = Object.assign(where, additinalWhere.param);
    }
    const query = db.createQueryBuilder(table)
        .leftJoinAndSelect(`${table}.from`, `${accountTable} as from`)
        .leftJoinAndSelect(`${table}.group`, `${tableGroup}`)
        .leftJoinAndSelect(`${table}.order`, todoOrderTable, `"${todoOrderTable}".type=:orderType`, { orderType: TodoOrderType.GROUP })
        .leftJoinAndSelect(`${table}.parent`, `parent`, `"parent"."tenantId"=:tenantId and "parent"."deletedAt" IS NULL`, { tenantId: tenant.id })
        .leftJoinAndSelect(`${table}.personnels`, `personnels`)
        .leftJoinAndSelect(`${table}.children`, `children`, `"children"."tenantId"=:tenantId and "children"."deletedAt" IS NULL`, { tenantId: tenant.id })
        .leftJoinAndSelect(`${table}.messageLinks`, 'messageLink', `"messageLink"."tenantId"=:tenantId and "messageLink"."deletedAt" IS NULL`, { tenantId: tenant.id })
        .leftJoinAndSelect(`${table}.files`, tableFile)
        .leftJoinAndSelect(`${table}.customAttributeValues`, tableCustomAttributeValue)
        .leftJoinAndSelect(`${tableCustomAttributeValue}.customAttribute`, tableCustomAttribute)
        .where(sqlWhere, where)
        .orderBy(`"${todoOrderTable}"."orderKey"`)
        .addOrderBy(`personnels.id`)
        .addOrderBy(`children.id`)
        .addOrderBy(`"${tableFile}".id`);
    if (limit) {
        query.limit(limit);
    }
    return await query.getMany();
}
Todo.loadChildrenToJoinPersonnels = async (tenant, todos) => {
    const childrenDictionaly = [];
    todos.forEach(t => t.children && t.children.forEach(c => childrenDictionaly[c.id] = c));
    const childrenIds = Object.keys(childrenDictionaly);
    if (childrenIds && childrenIds.length > 0) {
        const children = await db.createQueryBuilder(table)
            .leftJoinAndSelect(`${table}.from`, `${accountTable} as from`)
            .leftJoinAndSelect(`${table}.group`, `${tableGroup}`)
            .leftJoinAndSelect(`${table}.parent`, `parent`, `"parent"."tenantId"=:tenantId and "parent"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.personnels`, `personnels`)
            .leftJoinAndSelect(`${table}.children`, `children`, `"children"."tenantId"=:tenantId and "children"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.messageLinks`, 'messageLink', `"messageLink"."tenantId"=:tenantId and "messageLink"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.files`, tableFile, `"${tableFile}"."tenantId"=:tenantId and "${tableFile}"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.customAttributeValues`, tableCustomAttributeValue)
            .leftJoinAndSelect(`${tableCustomAttributeValue}.customAttribute`, tableCustomAttribute)
            .where(`${table}.id in (:...childrenIds) and ${table}."tenantId"=:tenantId`, { childrenIds, tenantId: tenant.id })
            .orderBy(`${table}.id`)
            .addOrderBy(`personnels.id`)
            .addOrderBy(`children.id`)
            .getMany();
        const idMap = [];
        children.forEach(t => idMap[t.id] = t);
        todos.forEach(t => {
            for (let i = 0; i < t.children.length; i++) {
                t.children[i] = idMap[t.children[i].id];
            }
        });
    }
}

module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Todo,
    columns: {
        title: {
            type: "varchar"
        },
        content: {
            type: "varchar",
            nullable: true
        },
        limitedAt: {
            type: "timestamp",
            nullable: true
        },
        groupId: {
            type: "int",
            nullable: true
        },
        progress: {
            type: "int",
            nullable: true
        },
        type: {
            type: "varchar",
            nullable: true
        },
        key: {
            type: "varchar",
            nullable: true
        },
        point: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        personnels: {
            target: accountTargetName,
            type: "many-to-many",
            joinTable: true,
            cascade: false,
            joinTable: {
                name: 'todo_perssonels_account',
                joinColumn: {
                    name: 'todoId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'todos',
            }
        },
        from: {
            target: accountTargetName,
            type: "many-to-one",
            joinColumn: true,
            cascade: false
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true
        },
        order: {
            target: todoOrderTargetName,
            type: "one-to-many",
            cascade: false,
            joinColumn: {
                name: 'id',
                referencedColumnName: 'todoId'
            },
            nullable: true,
            inverseSide: 'todo'
        },
        parent: {
            target: module.exports.targetName,
            type: "many-to-one",
            cascade: false,
            nullable: true,
            inverseSide: 'children'
        },
        children: {
            target: module.exports.targetName,
            type: "one-to-many",
            cascade: false,
            nullable: true,
            inverseSide: 'parent'
        },
        customAttributeValues: {
            target: customAttributeValueTargetName,
            type: "one-to-many",
            cascade: false,
            nullable: true,
            inverseSide: 'todo'
        },
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'todos'
        },
        files: {
            target: fileTargetName,
            type: "one-to-many",
            cascade: false,
            inverseSide: "todo",
        },
        messageLinks: {
            target: messageLinkargetName,
            type: "one-to-many",
            cascade: false,
            inverseSide: "todo",
        }
    },
    indices: [{ unique: false, columns: ["tenant", "group", "parent", "deletedAt"] }, { unique: false, columns: ["parent", "deletedAt"] }, { unique: false, columns: ["tenant"] }]
}, DefaultEntity));
