const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "custom_attribute_value";
class CustomAttributeValue {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.intValue = null;
        this.stringValue = null;
        this.dateValue = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    CustomAttributeValue,
    table,
    targetName: "CustomAttributeValue"
}
const { targetName: todoTargetName } = require("./Todo")
const { targetName: customAttributeTargetName } = require("./CustomAttribute")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: CustomAttributeValue,
    columns: {
        stringValue: {
            type: "varchar",
            nullable: true
        },
        intValue: {
            type: "int",
            nullable: true
        },
        dateValue: {
            type: "timestamp",
            nullable: true
        },
        todoId: {
            type: "int",
            nullable: true
        },
    },
    relations: {
        customAttribute: {
            target: customAttributeTargetName,
            type: "many-to-one",
            cascade: false
        },
        todo: {
            target: todoTargetName,
            type: "many-to-one",
            cascade: false,
            inverseSide: 'customAttributeValues'
        }
    },
    indices: [{ unique: false, columns: ["todo"] }]
}, DefaultEntity));