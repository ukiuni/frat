const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require("object-assign-deep");
const DefaultEntity = require("./DefaultEntity");
const table = "account"
class Account {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.name = null;
        this.mail = null;
        this.description = null;
        this.iconUrl = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    Account,
    table,
    targetName: "Account"
}
const db = require("../util/db");
const Strings = require("../util/stringUtil");
const generateUniqueName = async (mail) => {
    var name = mail.split("@")[0];
    var sameNameAccount = "dummy";
    var index = 0;
    while (sameNameAccount) {
        sameNameAccount = await db.findOne(null, table, `${table}.name = :name`, { name });
        if (sameNameAccount) {
            name = mail.split("@")[0] + "_" + index++;
        }
    }
    return name;
}
const { targetName: todoTargetName } = require("./Todo");
const { targetName: groupTargetName } = require("./Group");
const { targetName: accountTenantRelationTargetName } = require("./AccountTenantRelation");
const { table: tableAccountAccessKey, type: AccountAccessKeyType } = require("../model/AccountAccessKey");

Account.create = async (mail) => {
    var name = await generateUniqueName(mail);
    return await db.save(table, new Account({ mail, name }));
}
Account.loadAccountAccessKey = async (key, tenant) => {
    if (!key) {
        return null;
    }
    return await db.createQueryBuilder(tableAccountAccessKey).leftJoinAndSelect(tableAccountAccessKey + ".owner", table)
        .where(`${tableAccountAccessKey}."key" = :key and type=:type and "tenantId"=:tenantId and ${tableAccountAccessKey}."deletedAt" IS NULL`, { key: Strings.encrypt(key), type: AccountAccessKeyType.WEB, tenantId: tenant.id })
        .getOne();
}
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Account,
    columns: {
        name: {
            type: "varchar",
            unique: true
        },
        mail: {
            type: "varchar",
            unique: true
        },
        description: {
            type: "varchar",
            nullable: true
        },
        iconUrl: {
            type: "varchar",
            nullable: true
        },
        desktopNotification: {
            type: "boolean",
            nullable: true
        }
    },
    relations: {
        todos: {
            target: todoTargetName,
            type: "many-to-many",
            joinTable: true,
            cascade: false,
            joinTable: {
                name: 'todo_perssonels_account',
                joinColumn: {
                    name: 'accountId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'perssonels',
            }
        },
        groups: {
            target: groupTargetName,
            type: "many-to-many",
            joinTable: true,
            cascade: false,
            joinTable: {
                name: 'group_account',
                joinColumn: {
                    name: 'accountId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'members',
            }
        },
        viewGroups: {
            target: groupTargetName,
            type: "many-to-many",
            joinTable: true,
            cascade: false,
            joinTable: {
                name: 'view_group_account',
                joinColumn: {
                    name: 'accountId',
                    referencedColumnName: 'id'
                },
                inverseSide: 'viewMembers',
            }
        },
        tenantRelations: {
            target: accountTenantRelationTargetName,
            type: "one-to-many",
            joinTable: true,
            inverseSide: 'account',
        }
    }
}, DefaultEntity));

