const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "wiki";
class Wiki {
    constructor(deafultArgs) {
        //fields
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    Wiki,
    table,
    targetName: "Wiki"
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: tenantTargetName } = require("./Tenant");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Wiki,
    columns: {
        title: {
            type: "varchar"
        },
        content: {
            type: "varchar"
        },
        tenantId: {
            type: "int"
        },
        groupId: {
            type: "int"
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false
        },
        register: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true
        }
    },
    uniques: [{ name: "unique_title_per_group", columns: ["group", "title"] }]
}, DefaultEntity));