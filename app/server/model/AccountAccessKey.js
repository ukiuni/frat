const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "account_access_key"
class AccountAccessKey {
    constructor(deafultArgs) {
        //fields
        this.id = 0;
        this.type = null;
        this.key = null;
        this.description = null;
        this.owner = null;
        Object.assign(this, deafultArgs);
    }
}
module.exports = {
    AccountAccessKey,
    table,
    targetName: "AccountAccessKey",
    type: {
        LOGIN: "login",
        WEB: "web"
    }
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: tenantTargetName } = require("./Tenant")
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: AccountAccessKey,
    columns: {
        type: {
            type: "varchar"
        },
        key: {
            type: "varchar"
        },
        description: {
            type: "varchar",
            nullable: true
        }
    },
    relations: {
        owner: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        }
    }
}, DefaultEntity));

