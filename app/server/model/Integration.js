const EntitySchema = require("typeorm").EntitySchema;
const objectAssignDeep = require(`object-assign-deep`);
const DefaultEntity = require("./DefaultEntity");
const table = "integration";
class Integration {
    constructor(deafultArgs) {
    }
}
module.exports = {
    Integration,
    table,
    targetName: "Integration",
    type: {
        GITLAB: "GITLAB",
        AZURE_DEVOPS_INTEGRATION: "AZURE_DEVOPS",
        GITHUB: "GITHUB",
        TEAMS_INTEGRATION: "TEAMS",
        SLACK_INTEGRATION: "SLACK"
    }
}

const { targetName: accountTargetName } = require("./Account");
const { targetName: groupTargetName } = require("../model/Group");
const { targetName: tenantTargetName } = require("./Tenant");
module.exports.schema = new EntitySchema(objectAssignDeep({
    name: table,
    target: Integration,
    columns: {
        type: {
            type: "varchar"
        },
        key: {
            type: "varchar",
            nullable: true
        },
        sharedSecret: {
            type: "varchar",
            nullable: true
        },
        registerId: {
            type: "int"
        },
        groupId: {
            type: "int",
            nullable: true
        },
        tenantId: {
            type: "int",
            nullable: true
        }
    },
    relations: {
        tenant: {
            target: tenantTargetName,
            type: "many-to-one",
            cascade: false
        },
        register: {
            target: accountTargetName,
            type: "many-to-one",
            joinTable: true,
            cascade: false
        },
        group: {
            target: groupTargetName,
            type: "many-to-one",
            cascade: false,
            joinColumn: true,
            nullable: true,
            inverseSide: 'integrations'
        }
    }
}, DefaultEntity));