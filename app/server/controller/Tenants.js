
const { table: tableTenant } = require("../model/Tenant");
const { Account, table: tableAccount } = require("../model/Account");
const { AccountTenantRelation, table: tableAccountTenantRelation, role: AccountTenantRelationRole } = require("../model/AccountTenantRelation");
const { table: tableFile } = require("../model/File");
const { Tenant } = require("../model/Tenant");
const { Plan } = require("../model/Plan");
const { Todo, table: todoTable } = require("../model/Todo");
const db = require("../util/db");
const sendMail = require("../util/mail");
const i18n = require("../util/i18n");
const Storage = require("../util/storage");
const ADMIN_MAILS = process.env.ADMIN_MAILS ? process.env.ADMIN_MAILS.split(",") : [];
var ipRangeCheck = require("ip-range-check");

module.exports = function (app) {
    app.get('/api/tenants/keyExists/:key', async function (req, res) {
        var key = req.params.key;
        if (!key || !key.match(/^[a-z\-]{5,}$/g)) {
            res.status(400).send();
            return;
        }
        var tenant = await db.findOne(null, tableTenant, "key=:key", { key });
        res.send({ exists: !!tenant });
    });
    const loadSelfRelation = async (account, tenant) => {
        return await db.findOne(tenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: account.id, tenantId: tenant.id });
    }
    app.get('/api/tenants/current', async function (req, res) {
        var selfRelation = await loadSelfRelation(req.requestAccount, req.requestTenant);
        if (!selfRelation) {
            res.status(401).send();
            return;
        }
        var responseTenant = Object.assign({}, req.requestTenant);
        responseTenant.selfRelation = selfRelation;
        if (selfRelation.role == AccountTenantRelationRole.OWNER) {
            var tenantRelations = await Tenant.loadRelation(req.requestTenant);
            responseTenant.relations = tenantRelations.map(r => { r.account = { id: r.account.id, name: r.account.name, mail: r.account.mail, iconUrl: r.account.iconUrl }; return r; });
        }
        responseTenant.isSystemAdmin = ADMIN_MAILS.some(m => m == req.requestAccount.mail);
        const plan = await Plan.loadPlan(req.requestTenant);
        responseTenant.fileSizeLimit = responseTenant.fileSizeLimit || Plan.getFileSizeLimit(plan);
        responseTenant.groupCountLimit = responseTenant.groupCountLimit || Plan.getGroupCountLimit(plan);
        responseTenant.accountCountLimit = responseTenant.accountCountLimit || Plan.getAccountCountLimit(plan);
        res.send(responseTenant);
    });
    const isUpdatable = async (tenant, account) => {
        return await db.findOne(tenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId and "role"='OWNER'`, { accountId: account.id, tenantId: tenant.id });
    }
    app.put('/api/tenants/accessibleIpRanges', async function (req, res) {
        if (!await isUpdatable(req.requestTenant, req.requestAccount)) {
            res.status(401).send();
            return;
        }
        const ipRangesSrc = req.body.accessibleIpRanges;
        if (!ipRangesSrc) {
            req.requestTenant.accessibleIpRanges = null;
            await db.save(tableTenant, req.requestTenant);
        } else {
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            const ipRanges = ipRangesSrc.split(" ").join("").split(",");
            if (!ipRangeCheck(ip, ipRanges)) {
                res.status(400).send({ ip });
                return;
            }
            req.requestTenant.accessibleIpRanges = ipRanges.join(",");
            await db.save(tableTenant, req.requestTenant);
        }
        res.send();
    });
    app.put('/api/tenants/updateLimits', async function (req, res) {
        if (!ADMIN_MAILS.some(m => m == req.requestAccount.mail)) {
            res.status(401).send();
            return;
        }
        const { id, fileSizeLimit, groupCountLimit, accountCountLimit } = req.body;
        await db.save(tableTenant, { id, fileSizeLimit, groupCountLimit, accountCountLimit });
        res.send();
    });
    app.put('/api/tenants/accounts', async function (req, res) {
        var requestAccount = req.body;
        if (!requestAccount || (!requestAccount.mail && !requestAccount.id) || (requestAccount.mail == req.requestAccount.mail || requestAccount.id == req.requestAccount.id)) {
            res.status(400).send();
            return;
        }
        var selfRelation = await loadSelfRelation(req.requestAccount, req.requestTenant);
        if (!selfRelation || selfRelation.role != AccountTenantRelationRole.OWNER) {
            res.status(401).send();
            return;
        }
        var account = requestAccount.id ? await db.findById(tableAccount, requestAccount.id) : await db.findOne(null, tableAccount, "mail ILIKE :mail", { mail: requestAccount.mail });
        var currentAccountsCount = (await db.find(req.requestTenant, tableAccountTenantRelation, `"tenantId" = :tenantId and (role != 'REJECTOR' and role != 'REQUESTER') `, { tenantId: req.requestTenant.id })).length;
        if (!account) {
            currentAccountsCount++;
        }
        const plan = await Plan.loadPlan(req.requestTenant);
        const accountCountLimit = (req.requestTenant.accountCountLimit || Plan.getAccountCountLimit(plan));
        if (accountCountLimit < currentAccountsCount && !(account && requestAccount.role == AccountTenantRelationRole.REJECTOR)) {
            res.status(507).send({ error: "account count is limited to " + accountCountLimit + " and now " + currentAccountsCount, limit: accountCountLimit });
            return;
        }

        const onLimit = accountCountLimit == currentAccountsCount;
        if (account) {
            if (account.id == req.requestAccount.id) {
                res.status(401).send();
                return;
            }
            // if has account, 
            var accountTenantRelation = await db.findOne(req.requestTenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: account.id, tenantId: req.requestTenant.id });
            if (accountTenantRelation) {
                if (onLimit && accountTenantRelation.role == AccountTenantRelationRole.REQUESTER) {
                    res.status(507).send({ error: "account count is limited to " + accountCountLimit, limit: accountCountLimit });
                    return;
                };
                if (accountTenantRelation.role == AccountTenantRelationRole.INVITER) {
                    res.status(409).send();
                    return;
                }
                const checkedRole = req.body.role && Object.keys(AccountTenantRelationRole).some(r => r == req.body.role) ? req.body.role : null;
                if (!checkedRole) {
                    res.status(400).send();
                    return;
                }
                console.log(`log:changeRole\tkey:${req.tranzactionKey}\taccountId:${req.requestAccount.id}\ttargetUser:${account.id}\trole:${checkedRole}`);
                accountTenantRelation.role = checkedRole
                accountTenantRelation.account = account;
                accountTenantRelation.tenant = req.requestTenant;
                var updatedRelation = await db.save(tableAccountTenantRelation, accountTenantRelation);
                res.send(updatedRelation);
                return;
            }
        } else if (requestAccount.mail) {
            account = await Account.create(requestAccount.mail);
        } else {
            res.status(404).send({ message: "account not found" });
            return;
        }
        console.log(`log:invite\tkey:${req.tranzactionKey}\taccountId:${req.requestAccount.id}\ttargetUser:${account.id}`);
        var savedRelation = await db.save(tableAccountTenantRelation, new AccountTenantRelation({ account, tenant: req.requestTenant.id, role: AccountTenantRelationRole.INVITER }));
        savedRelation.account = { id: savedRelation.account.id, name: savedRelation.account.name, mail: savedRelation.account.mail }
        const lang = i18n.getClientRanguage(req);
        sendMail({
            subject: i18n.getMessage(lang, "tenant.mails.invite.subject"),
            to: requestAccount.mail,
            text: i18n.getMessage(lang, "tenant.mails.invite.text", { inviter: req.requestAccount.mail, protocol: req.protocol, host: req.get("Host") }),
            html: i18n.getMessage(lang, "tenant.mails.invite.html", { inviter: req.requestAccount.mail, protocol: req.protocol, host: req.get("Host") })
        });
        res.send(savedRelation);
    });
    app.delete('/api/tenants/accounts/notJoin/:id', async function (req, res) {
        var requestAccountId = req.params.id;
        if (!requestAccountId) {
            res.status(400).send();
            return;
        }
        var selfRelation = await loadSelfRelation(req.requestAccount, req.requestTenant);
        if (!selfRelation || selfRelation.role != AccountTenantRelationRole.OWNER) {
            res.status(401).send();
            return;
        }
        var accountTenantRelation = await db.findOne(req.requestTenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: requestAccountId, tenantId: req.requestTenant.id });
        if (!accountTenantRelation) {
            res.status(404).send();
            return;
        }
        if (!(accountTenantRelation.role == AccountTenantRelationRole.INVITER || accountTenantRelation.role == AccountTenantRelationRole.REQUESTER)) {
            res.status(409).send();
            return;
        }
        await db.deleteConcrete(tableAccountTenantRelation, accountTenantRelation);
        res.send(accountTenantRelation);
    })
    app.get('/api/tenants/files', async function (req, res) {
        var selfRelation = await loadSelfRelation(req.requestAccount, req.requestTenant);
        if (!selfRelation || selfRelation.role != AccountTenantRelationRole.OWNER) {
            res.status(401).send();
            return;
        }
        const info = await Tenant.loadFileInfo(req.requestTenant);
        const files = await db.createQueryBuilder(tableFile)
            .leftJoinAndSelect(`${tableFile}.register`, `register`, `"register"."deletedAt" IS NULL`)
            .leftJoinAndSelect(`${tableFile}.group`, "group", `"group"."deletedAt" IS NULL`)
            .leftJoinAndSelect(`${tableFile}.todo`, `todo`, `"todo"."deletedAt" IS NULL`)
            .where(`${tableFile}."tenantId"=:tenantId and "${tableFile}"."deletedAt" IS NULL`, { tenantId: req.requestTenant.id })
            .orderBy(`"${tableFile}"."createdAt"`, "DESC")
            .getMany();
        const plan = await Plan.loadPlan(req.requestTenant);
        info.tenantLimit = req.requestTenant.fileSizeLimit || Plan.getFileSizeLimit(plan);
        res.send({ info, files });
    });
    app.delete('/api/tenants/files/:id', async function (req, res) {
        if (!req.params.id) {
            res.status(400).send();
            return;
        }
        var selfRelation = await loadSelfRelation(req.requestAccount, req.requestTenant);
        if (!selfRelation || selfRelation.role != AccountTenantRelationRole.OWNER) {
            res.status(401).send();
            return;
        }
        const file = await db.findOne(req.requestTenant, tableFile, `${tableFile}."tenantId"=:tenantId and ${tableFile}.id = :fileId`, { tenantId: req.requestTenant.id, fileId: req.params.id })
        try {
            await Storage.delete(req.requestTenant.key, file.url);
        } catch (ignore) {
            console.log("error: can't delete s3 file." + ignore + ": " + req.requestTenant.key + "/" + file.url);
        }
        db.delete(tableFile, file)
        res.send(file);
    });
    app.get('/api/tenants', async function (req, res) {
        if (!ADMIN_MAILS.some(a => a == req.requestAccount.mail)) {
            res.status(401).send();
            return;
        }
        const tenantsAndFile = await db.getConnection().query(`select tenant.id as id, tenant.name as name, tenant.key as key, tenant."fileSizeLimit" as "fileSizeLimit", tenant."groupCountLimit" as "groupCountLimit", tenant."accountCountLimit" as "accountCountLimit", sum(file.size) as "fileSize" from tenant left join file on tenant.id = file."tenantId" and file."deletedAt" IS NULL group by tenant.id, tenant.key, tenant.name, tenant."fileSizeLimit" order by tenant.id`);
        const tenantsAndAccount = await db.getConnection().query(`select tenant.key as key, count(account_tenant_relation.id) as "accountCount" from tenant, account_tenant_relation where tenant.id = account_tenant_relation."tenantId" and account_tenant_relation."deletedAt" IS NULL and account_tenant_relation.role != 'REJECTOR' group by tenant.key`);
        const tenantsAndGroup = await db.getConnection().query(`select tenant.key as key, count("group".id) as "groupCount" from "group", tenant where "group"."tenantId" = tenant.id and "group"."deletedAt" IS NULL group by tenant.key`);
        tenantKeyAccountMap = [];
        tenantKeyGroupMap = [];
        tenantsAndAccount.forEach(t => tenantKeyAccountMap[t.key] = t);
        tenantsAndGroup.forEach(t => tenantKeyGroupMap[t.key] = t);

        const tenantPlanMap = [];
        (await Plan.loadAllPlan()).forEach(p => tenantPlanMap[p.tenantId] = p);
        tenantsAndFile.forEach(t => {
            t.accountCount = tenantKeyAccountMap[t.key] ? tenantKeyAccountMap[t.key].accountCount : 0;
            t.groupCount = tenantKeyGroupMap[t.key] ? tenantKeyGroupMap[t.key].groupCount : 0;
            const plan = tenantPlanMap[t.id];
            t.fileSizeLimit = t.fileSizeLimit || Plan.getFileSizeLimit(plan);
            t.accountCountLimit = t.accountCountLimit || Plan.getAccountCountLimit(plan);
            t.groupCountLimit = t.groupCountLimit || Plan.getGroupCountLimit(plan);
        });
        res.send(tenantsAndFile);
    });

    app.get('/api/tenants/todos/search', async function (req, res) {
        var searchWord = req.query.searchWord;
        if (!searchWord) {
            res.status(400).send();
            return;
        }
        var param = {}
        var sql = ` and (${todoTable}.title like :searchWord or ${todoTable}.content like :searchWord or ${todoTable}.key like :searchWord)`
        param.searchWord = `%${searchWord}%`;

        if (req.query.ignoreTodoIds) {
            sql += ` and "${todoTable}".id not in (:...ignoreTodoIds)`;
            param.ignoreTodoIds = req.query.ignoreTodoIds.split(",");
        }
        sql += `and ("group"."secret" = false or "group"."secret" is null)`
        const todos = await Todo.loadTodo(req.requestTenant, null, {
            param,
            sql
        }, 10)
        res.send(todos)
    })
};