const { Account, table } = require("../model/Account");
const { table: tableTenant, defaultDomains } = require("../model/Tenant");
const { table: tableAccountTenantRelation, role: AccountTenantRelationRole } = require("../model/AccountTenantRelation");
const { table: tableSprintBar } = require("../model/SprintBar");
const { AccountAccessKey, table: tableAccountAccessKey, type: AccountAccessKeyType } = require("../model/AccountAccessKey");
const { File, table: tableFile } = require("../model/File");
const db = require("../util/db");
const sendMail = require("../util/mail");
const Strings = require("../util/stringUtil");
const Storage = require("../util/storage");
const i18n = require("../util/i18n");
var ipRangeCheck = require("ip-range-check");
const AUTH_HEADER = "Authentication";
const LOGIN_KEY_LIMIT = 10 * 60 * 1000;

const controller = function (app) {
    const deleteAllLoginKey = async (tenant, ownerId) => {
        var oldLoginAccessKeys = await db.find(tenant, tableAccountAccessKey, '"ownerId"=:ownerId and type=:type', { ownerId, type: AccountAccessKeyType.LOGIN });
        await Promise.all(oldLoginAccessKeys.map(async (key) => {
            return db.delete(tableAccountAccessKey, key);
        }));
    }
    app.post('/api/accounts/requestSendLoginKey', async function (req, res) {
        var account = req.body;
        if (!account.mail || !Strings.isMail(account.mail)) {
            res.status(400).send();
            return;
        }
        var registedAccount = await db.findOne(null, table, `${table}.mail ILIKE :mail`, account);
        if (!registedAccount) {
            registedAccount = await Account.create(account.mail);
        }
        await deleteAllLoginKey(req.requestTenant, registedAccount.id);
        const key = Strings.randomString(10);
        var loginAccessKey = new AccountAccessKey({ type: AccountAccessKeyType.LOGIN, key, owner: registedAccount });
        await db.save(tableAccountAccessKey, loginAccessKey);
        const lang = i18n.getClientRanguage(req);
        await sendMail({
            subject: i18n.getMessage(lang, "login.mails.sendLoginCode.subject"),
            to: account.mail,
            text: i18n.getMessage(lang, "login.mails.sendLoginCode.text", { key }),
            html: i18n.getMessage(lang, "login.mails.sendLoginCode.html", { key, protocol: req.protocol, host: req.get("Host"), mail: encodeURIComponent(account.mail), subdomainWithParam: req.body.subdomain ? '&subdomain=' + req.body.subdomain : '' })
        });

        res.send(account);
    })
    app.post('/api/accounts/login', async function (req, res) {
        const account = req.body;
        if (!account.mail || !account.key) {
            res.status(400).send();
            return;
        }
        var registedAccount = await db.findOne(null, table, "mail ILIKE :mail", account);
        if (!registedAccount) {
            res.status(400).send();
            return;
        }
        var keyCreatedAt = new Date(new Date().getTime() - LOGIN_KEY_LIMIT);
        var loginKey = await db.findOne(null, tableAccountAccessKey, '"ownerId"=:ownerId and type=:type and "createdAt" >= :keyCreatedAt and key=:key', { ownerId: registedAccount.id, type: AccountAccessKeyType.LOGIN, keyCreatedAt, key: account.key });
        if (!loginKey) {
            res.status(400).send();
            return;
        }
        var withCreateTenant = !!account.subdomain;
        var isTheUserFirstJoin = false;
        if (withCreateTenant) {
            if (!account.subdomain.match(/^[a-z\-]{5,}$/g)) {
                res.status(400).send();
                return;
            }
            try {
                req.requestTenant = await db.save(tableTenant, { key: account.subdomain });
                await Storage.createBucket(req.requestTenant.key);
                await db.save(tableAccountTenantRelation, { account: registedAccount, tenant: req.requestTenant, role: AccountTenantRelationRole.OWNER })
            } catch (conflict) {
                let relation = await db.findOne(null, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId and "role"=:role`, { accountId: registedAccount.id, tenantId: req.requestTenant.id, role: AccountTenantRelationRole.OWNER });
                if (!relation) {
                    res.status(409).send();
                    return;
                }
            }
        } else {
            if (!req.requestTenant) {
                res.status(404).send({ message: "tenant unknown" });
                return;
            }
            var relation = await db.findOne(req.requestTenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: registedAccount.id, tenantId: req.requestTenant.id });
            if (!relation) {
                if (defaultDomains.some(d => d == req.requestTenant.key)) {
                    await db.save(tableAccountTenantRelation, { account: registedAccount, tenant: req.requestTenant, role: AccountTenantRelationRole.MEMBER })
                } else {
                    await db.save(tableAccountTenantRelation, { account: registedAccount, tenant: req.requestTenant, role: AccountTenantRelationRole.REQUESTER })
                    await deleteAllLoginKey(req.requestTenant, registedAccount.id);
                    res.status(423).send();
                    return;
                }
            } else if (relation.role == AccountTenantRelationRole.INVITER) {
                relation.role = AccountTenantRelationRole.MEMBER;
                relation.account = registedAccount;
                relation.tenant = req.requestTenant;
                await db.save(tableAccountTenantRelation, relation);
                isTheUserFirstJoin = true;
            } else if (relation.role == AccountTenantRelationRole.REQUESTER) {
                // if only one account. by some error???
                var relations = await db.find(req.requestTenant, tableAccountTenantRelation, `"tenantId"=:tenantId`, { tenantId: req.requestTenant.id });
                if (relations.length == 1 && relations[0].id == relation.id) {
                    await db.save(tableAccountTenantRelation, { id: relations[0].id, accountId: registedAccount.id, tenantId: req.requestTenant.id, role: AccountTenantRelationRole.OWNER });
                } else {
                    res.status(423).send();
                    return;
                }
            }
        }
        await deleteAllLoginKey(req.requestTenant, registedAccount.id);

        const key = Strings.randomString(20);
        const accessKey = await db.save(tableAccountAccessKey, new AccountAccessKey({ key: Strings.encrypt(key), type: AccountAccessKeyType.WEB, owner: registedAccount, tenant: req.requestTenant }));
        const lang = i18n.getClientRanguage(req);
        await sendMail({
            subject: i18n.getMessage(lang, "login.mails.login.subject"),
            to: account.mail,
            text: i18n.getMessage(lang, "login.mails.login.text", { userAgent: req.headers['user-agent'] }),
            html: i18n.getMessage(lang, "login.mails.login.html", { userAgent: req.headers['user-agent'], host: req.protocol + "://" + req.get("Host") })
        });
        accessKey.key = key;
        res.status(isTheUserFirstJoin ? 201 : 200).send(accessKey);
    })
    app.post('/api/accounts/logout', async function (req, res) {
        const accessKey = await db.findOne(req.requestTenant, tableAccountAccessKey, `${tableAccountAccessKey}.key = :key`, { key: Strings.encrypt(req.body.key) });
        await db.delete(tableAccountAccessKey, accessKey);
        res.status(200).send();
    })
    app.get('/api/accounts/self', async function (req, res) {
        if (req.requestAccount) {
            res.send(req.requestAccount);
        } else {
            res.status(404).send();
        }
    })
    app.put('/api/accounts/self', async function (req, res) {
        if (req.requestAccount) {
            req.requestAccount.iconUrl = req.body.iconUrl;
            req.requestAccount.desktopNotification = req.body.desktopNotification;
            await db.save(table, req.requestAccount);
            res.send(req.requestAccount);
        } else {
            res.status(404).send();
        }
    })
    app.get('/api/accounts/byName/:name', async function (req, res) {
        if (!req.params.name || req.params.name.length < 3) {
            res.status(400).send();
        }
        var accountRelations = await db.createQueryBuilder(tableAccountTenantRelation)
            .innerJoinAndSelect(`${tableAccountTenantRelation}.account`, `account`, `"account".name ilike :name`, { name: `%${req.params.name}%` })
            .where('role in (:...roles) and "tenantId"=:tenantId', { roles: [AccountTenantRelationRole.MEMBER, AccountTenantRelationRole.OWNER], tenantId: req.requestTenant.id })
            .limit(5)
            .getMany();
        var accounts = accountRelations.map(a => { return { id: a.account.id, name: a.account.name, iconUrl: a.account.iconUrl } });
        res.send(accounts);
    })
    app.get('/api/accounts/self/sprintBars', async function (req, res) {
        res.send(await db.find(req.requestTenant, tableSprintBar, `"accountId" = :accountId and "deletedAt" IS NULL`, { accountId: req.requestAccount.id }));
    })
    app.post('/api/accounts/self/sprintBars', async function (req, res) {
        var title = req.body.title;
        var orderKey = req.body.orderKey;
        if (!orderKey) {
            res.status(400).send();
            return;
        }
        res.send(await db.save(tableSprintBar, { tenant: req.requestTenant, account: req.requestAccount, title, orderKey }));
    })
    app.put('/api/accounts/self/sprintBars', async function (req, res) {
        var id = req.body.id;
        var title = req.body.title;
        if (!id || !title) {
            res.status(400).send();
            return;
        }
        var sprintBar = await db.findOne(req.requestTenant, tableSprintBar, `id = :id and "accountId" = :accountId`, { id, accountId: req.requestAccount.id })
        if (!sprintBar) {
            res.status(404).send();
            return;
        }
        sprintBar.title = title;
        sprintBar.startAt = Strings.toDate(req.body.startAt);
        sprintBar.endAt = Strings.toDate(req.body.endAt);
        res.send(await db.save(tableSprintBar, sprintBar))
    })
    app.delete('/api/accounts/self/sprintBars/:id', async function (req, res) {
        var id = req.params.id;
        var sprintBar = await db.findOne(req.requestTenant, tableSprintBar, `id = :id and "accountId" = :accountId and "groupId" IS NULL`, { id, accountId: req.requestAccount.id })
        if (!sprintBar) {
            res.status(404).send();
            return;
        }
        await db.delete(tableSprintBar, sprintBar);
        res.send();
    })
    app.post('/api/account/selfImage/uploadUrl', async function (req, res) {
        if (req.body.fileSize > 1000000) {
            res.status(400).send({ message: "too large" });
            return;
        }
        var uploadUrl = await Storage.getAccountIconUploadUrl(Strings.randomString(50) + "/" + req.body.fileName, req.body.fileSize);
        var file = new File({ tenant: req.requestTenant, register: req.requestAccount, size: req.body.fileSize, name: req.body.fileName, url: uploadUrl.url + "/" + Strings.fixedEncodeURIComponent(uploadUrl.fields.key).replace(/%2F/g, "/") });
        await db.save(tableFile, file);
        uploadUrl.file = file;
        res.send(uploadUrl);
    })
}
var responseAccessError = ({ req, res, host, ip, tranzactionKey, status, body }) => {
    console.log(`log:rejectAccess\tkey:${tranzactionKey}\taccountId:${req.requestAccount ? req.requestAccount.id : 0}\thost:${host}\tmethod:${req.method}\tpath:${req.path}\tip:${ip}`);//TODO accessLog
    res.status(status || 401).send(body);
}
controller.accountAuthMiddleWare = async (req, res, next) => {
    var host = req.get("Host");
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var tranzactionKey = Strings.randomString(10);
    req.tranzactionKey = tranzactionKey;
    try {
        req.requestAccount = null;

        var subdomain = host.split(".")[0].split(":")[0];
        req.requestTenant = await db.findOne(null, tableTenant, "key=:key", { key: subdomain });
        if (req.requestTenant && req.requestTenant.accessibleIpRanges && req.path.indexOf("/webhook/groups/") != 0) {
            if (!ipRangeCheck(ip, req.requestTenant.accessibleIpRanges.split(","))) {
                responseAccessError({ req, res, host, ip, tranzactionKey, status: 403 });
                return;
            }
        }
        if (req.path.indexOf("/api/") == 0
            && req.path.indexOf("/api/accounts/login") != 0
            && req.path.indexOf("/api/accounts/requestSendLoginKey") != 0
            && req.path.indexOf("/api/tenants/keyExists/") != 0) {
            if (!req.requestTenant) {
                responseAccessError({ req, res, host, ip, tranzactionKey, status: 423, body: { message: "requesting" } });
                return;
            }
            const accessKeySource = req.get(AUTH_HEADER);
            if (!accessKeySource) {
                responseAccessError({ req, res, host, ip, tranzactionKey, status: 423, body: { message: "requesting" } });
                return;
            }
            const accountAccessKey = await Account.loadAccountAccessKey(accessKeySource, req.requestTenant);
            if (accountAccessKey && req.requestTenant) {
                var selfRelation = await db.findOne(req.requestTenant, tableAccountTenantRelation, '"accountId"=:accountId and "tenantId"=:tenantId and "role" in (:...roles)', { accountId: accountAccessKey.owner.id, tenantId: req.requestTenant.id, roles: [AccountTenantRelationRole.OWNER, AccountTenantRelationRole.MEMBER] })
                if (!selfRelation) {
                    responseAccessError({ req, res, host, ip, tranzactionKey, status: 423, body: { message: "requesting" } });
                    return;
                }
                req.requestAccount = accountAccessKey.owner;
            } else {
                responseAccessError({ req, res, host, ip, tranzactionKey });
                return;
            }
        }
        var startTime = new Date().getTime();
        console.log(`log:access\tkey:${tranzactionKey}\taccountId:${req.requestAccount ? req.requestAccount.id : 0}\thost:${host}\tmethod:${req.method}\tsubdomain:${subdomain}\tpath:${req.path}\tip:${ip}`);
        res.once("finish", function () {
            var cost = new Date().getTime() - startTime;
            console.log(`log:afterAccess\tkey:${tranzactionKey}\tmethod:${req.method}\tpath:${req.path}\tstatus:${res.statusCode}\tcost:${cost}`);//TODO accessLog
            req.requestAccount = null;
        });
        next();
    } catch (e) {
        console.log(`log:globalError\tkey:${tranzactionKey}\taccountId:${req.requestAccount ? req.requestAccount.id : 0}\thost:${host}\tmethod:${req.method}\tsubdomain:${subdomain}\tpath:${req.path}\tip:${ip}\terr:${e}\tstack:${e.stack}`);
        res.status(500).send();
    }
}
module.exports = controller;