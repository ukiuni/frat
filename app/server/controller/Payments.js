const payjsSkey = process.env.PAYJS_SKEY || "sk_test_f768db1f4e5298c53b0f2edc";
const payjsPkey = process.env.PAYJS_PKEY || "pk_test_c88bde9ebbcfbcfdd33b19b1";
const DEACTIVE_PAYMENT = process.env.DEACTIVE_PAYMENT == "true";
const payjp = require('payjp')(payjsSkey);
const db = require("../util/db");
const { Plan, table: tablePlan } = require("../model/Plan");
const { Group } = require("../model/Group");
const { Tenant } = require("../model/Tenant");
const { table: tableAccountTenantRelation, role: AccountTenantRelationRole } = require("../model/AccountTenantRelation");


module.exports = function (app) {
    app.get('/api/tenants/plan', async (req, res) => {
        var plan = await Plan.loadPlan(req.requestTenant);
        if (!plan) {
            plan = {};
        }
        if (plan.payerId != req.requestAccount.id) {
            delete plan.cardTokenId;
        }
        var relation = await db.findOne(req.requestTenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: req.requestAccount.id, tenantId: req.requestTenant.id });
        plan.editable = AccountTenantRelationRole.OWNER == relation.role && !DEACTIVE_PAYMENT;
        plan.payjsPublicKey = payjsPkey;
        res.send(plan);
    })
    const canChangePlanTo = async (tenant, plan) => {
        const fileSizeTotal = (await Tenant.loadFileInfo(tenant)).sum;
        const groupCount = (await Group.openGroupInTenant(tenant)).length;
        const tenantRelations = await Tenant.loadRelation(tenant);
        const memberCount = tenantRelations.filter(r => AccountTenantRelationRole.REJECTOR != r.role).length;

        if ("premium" == plan) {
            return;
        } else if ("standard" == plan) {
            if (fileSizeTotal > 30000000000) {
                return false;
            }
            if (groupCount > 10) {
                return false;
            }
            if (memberCount > 100) {
                return false;
            }
        } else {
            if (fileSizeTotal > 1000000000) {
                return false;
            }
            if (groupCount > 2) {
                return false;
            }
            if (memberCount > 5) {
                return false;
            }
        }
        return true;
    }
    app.put('/api/tenants/plan', async (req, res) => {
        var relation = await db.findOne(req.requestTenant, tableAccountTenantRelation, `"accountId"=:accountId and "tenantId"=:tenantId`, { accountId: req.requestAccount.id, tenantId: req.requestTenant.id });
        if (AccountTenantRelationRole.OWNER != relation.role || DEACTIVE_PAYMENT) {
            res.status(401).send();
            return;
        }

        const cardTokenId = req.body.cardTokenId;
        const plan = req.body.plan;

        if (!(plan == 'standard' || plan == 'premium' || plan == 'free')) {
            res.status(400).send();
            return;
        }
        if (!canChangePlanTo(req.requestTenant, plan)) {
            res.status(403).send();
            return;
        }

        if (plan == 'standard' || plan == 'premium') {
            try {
                let cardInfo = await payjp.tokens.retrieve(cardTokenId);
                if (cardInfo.error || cardInfo.card.cvc_check != "passed") {
                    res.status(400).send();
                    return;
                }
            } catch (e) {
                res.status(400).send();
                return;
            }
        }
        var createdPlan;
        try {
            await db.transaction(async (q) => {
                // treat self db
                // drop all exists plan
                var time = new Date();
                const currentPlans = await q.manager.getRepository(tablePlan).createQueryBuilder(tablePlan).where('"tenantId" = :tenantId and "deletedAt" IS NULL', { tenantId: req.requestTenant.id }).getMany();
                if (currentPlans.length != 0) {
                    currentPlans.forEach(p => {
                        p.updatedAt = time;
                        p.deletedAt = time;
                    })
                    await q.manager.getRepository(tablePlan).save(currentPlans);
                }
                // create new plan
                createdPlan = await q.manager.getRepository(tablePlan).save(new Plan({ createdAt: time, cardToken: cardTokenId, plan, tenant: req.requestTenant, payer: req.requestAccount }));

                // treat pay jp
                // setup customer and card
                var customer
                try {
                    customer = await payjp.customers.retrieve(req.requestTenant.id);
                    if (cardTokenId) {
                        try {
                            await payjp.customers.update(req.requestTenant.id, {
                                default_card: cardTokenId
                            });
                        } catch (e) {
                            console.log("error on retrieve and update cards" + e + ":" + e.stack);
                            await payjp.customers.cards.create(req.requestTenant.id, {
                                card: cardTokenId,
                                default: true,
                            });
                        }
                    }
                } catch (e) {
                    console.log("error on get customer or card" + e + ":" + e.stack);
                }
                if (!customer) {
                    let param = {
                        id: req.requestTenant.id
                    }
                    if (cardTokenId) {
                        param.card = cardTokenId;
                    }
                    customer = await payjp.customers.create(param);
                    if (customer.error) {
                        throw customer.error
                    }
                    existpayments = [];
                }

                //revoke exists payments
                var existpayments = await payjp.subscriptions.list(req.requestTenant.id);
                if (existpayments.count != 0) {
                    await Promise.all(existpayments.data.map(payment => payjp.subscriptions.delete(payment.id, { prorate: true })));
                }

                if (plan == 'standard' || plan == 'premium') {
                    // create new payment
                    const subscription = await payjp.subscriptions.create({
                        plan: plan,
                        customer: customer.id,
                        prorate: true
                    });
                    if (subscription.error) {
                        throw subscription.error;
                    }
                }
            })

            res.send(createdPlan);
        } catch (e) {
            console.log(`error on payment tenantid:${req.requestTenant.id}, account:${req.requestAccount.mail}, plan:${plan}, ${e}\n${e.stack}`);
            res.status(400).send();
        }
    })
}