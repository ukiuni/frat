const { Todo, table, type: TodoType } = require("../model/Todo");
const { table: tableTodoOrder, type: TodoOrderType, TodoOrder } = require("../model/TodoOrder");
const { table: tableAccount } = require("../model/Account");
const { table: tableGroup, Group } = require("../model/Group");
const { table: tableFile } = require("../model/File");
const { table: tableTodoTodoRelation, targetName: todoTodoRelationTargetName } = require("../model/TodoTodoRelation");
const { table: tableCustomAttributeValue } = require("../model/CustomAttributeValue");
const { table: tableCustomAttribute } = require("../model/CustomAttribute");
const { table: tableSprintBar } = require("../model/SprintBar");
const { Event, table: tableEvent, type: EventType } = require("../model/Event");
const { table: tableChatMessage } = require("../model/ChatMessage");
const ChatSender = require("./Chat").sender;
const calcOrderKey = require("../util/calcOrderKey")

const Strings = require("../util/stringUtil");
const BigNumber = require('bignumber.js');
BigNumber.config({ RANGE: [-1000, 1], EXPONENTIAL_AT: [-1000, 1], DECIMAL_PLACES: 1000, ALPHABET: '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' })
const db = require("../util/db");
const defaultAndIncrementsStringValue = "0.00000000000000000000000000000000001";
const ORDERDEFAULT = new BigNumber(defaultAndIncrementsStringValue);
const ORDERINCREMENTS = new BigNumber(defaultAndIncrementsStringValue);
const replaceDoubleQuart = (src) => {
    if (!src) {
        return src;
    }
    return src.split("\"").join("");
}
module.exports = function (app) {
    app.get('/api/todos/self', async function (req, res) {
        const tenant = req.requestTenant;
        const account = await db.createQueryBuilder(tableAccount)
            .leftJoinAndSelect(`${tableAccount}.todos`, table, `"${table}"."tenantId"=:tenantId and "${table}"."deletedAt" IS NULL`, { tenantId: req.requestTenant.id })
            .leftJoinAndSelect(`${table}.from`, `${tableAccount} as from`)
            .leftJoinAndSelect(`${table}.group`, `${tableGroup}`)
            .leftJoinAndSelect(`${table}.order`, tableTodoOrder, `"${tableTodoOrder}".type=:orderType`, { orderType: TodoOrderType.SELF })
            .leftJoinAndSelect(`${table}.parent`, `parent`, `"parent"."tenantId"=:tenantId and "parent"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.personnels`, `personnels`)
            .leftJoinAndSelect(`${table}.children`, `children`, `"children"."tenantId"=:tenantId and "children"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.messageLinks`, 'messageLink', `"messageLink"."tenantId"=:tenantId and "messageLink"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.files`, tableFile, `"${tableFile}"."tenantId"=:tenantId and "${tableFile}"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.customAttributeValues`, tableCustomAttributeValue)
            .leftJoinAndSelect(`${tableCustomAttributeValue}.customAttribute`, tableCustomAttribute)
            .where(`${tableAccount}.id = :id and "${tableGroup}"."deletedAt" IS NULL`, { id: req.requestAccount.id })
            .orderBy(`"${tableTodoOrder}"."orderKey"`)
            .addOrderBy(`personnels.id`)
            .addOrderBy(`children.id`)
            .addOrderBy(`"${tableFile}".id`)
            .getOne();
        //TODO in fact, parent has self account as personnel must be remove from parent by sql, caz duplicate. but parent personnels not join below.
        //so now, avoid duplicate with js.abnf
        let childrenIds = [];
        account.todos.forEach(t => t.children.forEach(c => childrenIds.push(c.id)));
        let todos = account.todos.filter(t => !childrenIds.find(id => t.id == id));

        await Todo.loadChildrenToJoinPersonnels(req.requestTenant, todos);
        res.send(todos);
    })
    var queryTodoWithPersonnelAndGroup = async (tenant, id) => {
        var idisIdNotKey = Strings.isNumberFormat(id);
        var idQuery = idisIdNotKey ? `${table}.id = :id` : `${table}.key = :id`
        const todo = await db.createQueryBuilder(table)
            .leftJoinAndSelect(`${table}.from`, `from`)
            .leftJoinAndSelect(`${table}.group`, `${tableGroup}`)
            .leftJoinAndSelect(`${table}.order`, tableTodoOrder)
            .leftJoinAndSelect(`${table}.parent`, `parent`, `"parent"."tenantId"=:tenantId and "parent"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.personnels`, `personnels`)
            .leftJoinAndSelect(`${table}.children`, `children`, `"children"."tenantId"=:tenantId and "children"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.messageLinks`, 'messageLink', `"messageLink"."tenantId"=:tenantId and "messageLink"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.files`, tableFile, `"${tableFile}"."tenantId"=:tenantId and "${tableFile}"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .leftJoinAndSelect(`${table}.customAttributeValues`, tableCustomAttributeValue)
            .leftJoinAndSelect(`${tableCustomAttributeValue}.customAttribute`, tableCustomAttribute)
            .where(idQuery + ` and ${table}."tenantId"=:tenantId and ${table}."deletedAt" IS NULL`, { id, tenantId: tenant.id })
            .orderBy(`"${tableTodoOrder}"."orderKey"`)
            .addOrderBy(`personnels.id`)
            .addOrderBy(`children.id`)
            .addOrderBy(`"${tableFile}".id`)
            .getOne();
        if (!todo) {
            return todo;
        }
        const relations = await db.createQueryBuilder(tableTodoTodoRelation)
            .leftJoinAndSelect(`${tableTodoTodoRelation}.relationTodo`, `relationTodo`, `"relationTodo"."tenantId"=:tenantId and "relationTodo"."deletedAt" IS NULL`, { tenantId: tenant.id })
            .where(`${tableTodoTodoRelation}.relatedTodoId = :id and "relationTodo"."tenantId"=:tenantId and "relationTodo"."deletedAt" IS NULL`, { id: todo.id, tenantId: tenant.id })
            .orderBy(`"relationTodo".id`)
            .getMany();
        todo.relations = relations.map(r => r.relationTodo);
        await Todo.loadChildrenToJoinPersonnels(tenant, [todo])
        return todo;
    }
    var checkTodoAccessible = async (req, res, todo, after) => {
        if (!todo) {
            res.status(400).send();
            return;
        }

        var group;
        if (todo.groupId) {
            group = await Group.findGroupWithMember(req.requestTenant, todo.groupId);
        }
        //check children todo has not group is not secret && not same group with parent.
        const joinedTodoIds = [todo.parent, todo.children, todo.relations].filter(t => t).flat().filter(t => t).map(t => parseInt(t.id));
        if (joinedTodoIds && joinedTodoIds.length > 0) {
            var query = `select 1 from todo, "group" where todo.id in (${joinedTodoIds.join(",")}) and todo."groupId" = "group".id and "group".secret = true `
            if (group) {
                query += ' and "group".id !=' + group.id;
            }
            var todoInSecretAndOtherGroup = await db.getConnection().query(query);
            if (todoInSecretAndOtherGroup && todoInSecretAndOtherGroup.length > 0) {
                res.status(401).send();
                return;
            }
        }
        if (todo.groupId && !await Group.isMember(req.requestTenant, todo.groupId, req.requestAccount, group)) {
            res.status(401).send();
            return;
        } else if (todo.files && (await Promise.all(todo.files.map(async (f) => await db.findById(tableFile, f.id)))).some(f => !((todo.groupId && f.groupId == todo.groupId) || f.registerId == req.requestAccount.id))) {
            res.status(401).send({ message: "wrong file" });
            return;
        } else if (todo.customAttributeValues && (await Promise.all(todo.customAttributeValues.filter(c => c.id).map(async c => await db.findById(tableCustomAttributeValue, c.id)))).some(c => c.todoId != todo.id)) {
            res.status(401).send({ message: "wrong custom attribute value" });
            return;
        } else if (todo.id) {
            if (!todo.groupId && !todo.personnels.find(p => p.id == req.requestAccount.id) && (!todo.from || todo.from.id != req.requestAccount.id)) {
                res.status(401).send();
                return;
            }
        }
        after(todo, group);
    }
    app.get('/api/todos/:id', async function (req, res) {
        var todo = await queryTodoWithPersonnelAndGroup(req.requestTenant, req.params.id);
        if (!todo) {
            res.status(404).send();
            return;
        }
        await checkTodoAccessible(req, res, todo, (todo) => {
            res.send(todo);
        });
    })
    app.delete('/api/todos/:id', async function (req, res) {
        var todo = await queryTodoWithPersonnelAndGroup(req.requestTenant, req.params.id);
        if (!todo) {
            res.status(404).send();
            return;
        }
        await checkTodoAccessible(req, res, todo, async (todo) => {
            await db.delete(table, todo);
            const event = new Event({ tenant: req.requestTenant, todo, owner: req.requestAccount, type: EventType.TODO_DELETE, source: todo });
            if (todo.groupId) {
                event.groupId = todo.groupId;
            }
            await db.save(tableEvent, event)
            if (todo.groupId) {
                ChatSender.sendToGroup(ChatSender.type.DELETE_TODO, todo);
            }
            res.send(todo);
        });
    })
    var save = async (req, res, beforeSave, afterSave) => {
        var todo = req.body;
        if (todo.group) {
            todo.groupId = todo.group.id;
        }
        delete todo.group;
        var isCreate = !todo.id;
        await checkTodoAccessible(req, res, todo, async (todo, group) => {
            if (!isCreate) {
                todoBeforeSave = await queryTodoWithPersonnelAndGroup(req.requestTenant, todo.id);
                if (todo.key != todoBeforeSave.key) {
                    res.status(400).send();
                    return;
                }
            }
            if (beforeSave) {
                await beforeSave(todo);
            }
            const originalPersonnels = todo.personnels;
            if (isCreate && todo.type == TodoType.MEMBER_TODO_PARENT && originalPersonnels.length > 1) {
                todo.personnels = [req.requestAccount];
            }
            todo.tenant = req.requestTenant;
            var todoBeforeSave;
            var originKey = todo.key;
            //if has order, TypeOrm deletes other order. so escape
            var order = todo.order;
            delete todo.order;
            var savedTodo = await db.save(table, todo);
            todo.key = originKey;
            const requestTenant = req.requestTenant;// use for after response;
            const requestAccount = req.requestAccount;// use for after response;
            todo.order = order;

            var savedChildren = null;
            if (isCreate && todo.type == TodoType.MEMBER_TODO_PARENT && originalPersonnels.length > 1) {
                savedChildren = [];
                var saveChildren = originalPersonnels.map(p => new Todo({ tenant: requestTenant, groupId: todo.groupId, parent: todo, type: TodoType.MEMBER_TODO_CHILD, personnels: [p], limit: todo.limit, title: todo.title + `[${p.name}]`, from: todo.from }));
                await Promise.all(saveChildren.map(async t => {
                    const savedTodo = await db.save(table, t)
                    const event = new Event({ tenant: requestTenant, todo: savedTodo, owner: requestAccount, type: EventType.TODO_CREATE, source: savedTodo });
                    if (savedTodo.groupId) {
                        event.groupId = savedTodo.groupId;
                    }
                    await db.save(tableEvent, event)
                    return savedChildren.push(savedTodo);
                }));
            }
            if (todo.customAttributeValues) {
                todo.customAttributeValues.forEach(cav => {
                    cav.todo = savedTodo;
                });
                await db.save(tableCustomAttributeValue, todo.customAttributeValues);
            }
            if (todo.files) {
                todo.files.forEach(f => {
                    f.todo = savedTodo;
                });
                await db.save(tableFile, todo.files);
            }
            await db.createQueryBuilder(tableTodoTodoRelation).delete()
                .from(todoTodoRelationTargetName)
                .where(`"relatedTodoId" = :id`, { id: savedTodo.id })
                .execute();
            if (todo.relations && todo.relations.length > 0) {
                var createdAt = new Date();
                await db.createQueryBuilder(tableTodoTodoRelation)
                    .insert()
                    .into(todoTodoRelationTargetName)
                    .values(todo.relations.map((t, order) => { return { relatedTodoId: savedTodo.id, relationTodoId: t.id, order, createdAt } }))
                    .execute();
            }
            if (afterSave) {
                await afterSave(savedTodo, saveChildren);
            }

            if (todo.customAttributeValues) {
                todo.customAttributeValues.forEach(cav => cav.todo = null);
            }
            if (todo.files) {
                todo.files.forEach(f => f.todo = null);
            }
            //start event
            var registedTodo = await queryTodoWithPersonnelAndGroup(requestTenant, todo.id);
            var targetTodoEvent;
            if (isCreate) {
                targetTodoEvent = new Event({ tenant: requestTenant, todo: registedTodo, owner: requestAccount, type: EventType.TODO_CREATE, source: registedTodo });
                if (todo.groupId) {
                    targetTodoEvent.groupId = todo.groupId;
                }
                await db.save(tableEvent, targetTodoEvent)
            } else {
                targetTodoEvent = new Event({ tenant: requestTenant, todo: registedTodo, owner: requestAccount, type: EventType.TODO_UPDATE, source: registedTodo, before: todoBeforeSave });
                if (todo.groupId) {
                    targetTodoEvent.groupId = todo.groupId;
                }
                await db.save(tableEvent, targetTodoEvent);
            }
            //end event
            res.send(registedTodo);
            if (registedTodo.group) {
                var pushTodo = Object.assign({ lastEvent: targetTodoEvent.summary }, registedTodo);
                ChatSender.sendToGroup(ChatSender.type.UPDATE_TODO, pushTodo);
            }
            //start joined todo event
            const beforeParentKey = todoBeforeSave && todoBeforeSave.parent && todoBeforeSave.parent.key;
            const registedTodoParentKey = registedTodo.parent && registedTodo.parent.key;
            if (beforeParentKey != registedTodoParentKey) {
                const registParentChangeEvent = async (todo, from) => {
                    const event = new Event({
                        tenant: requestTenant, todo: todo.parent, owner: requestAccount, type: EventType.TODO_UPDATE, summary:
                        {
                            type: EventType.TODO_UPDATE,
                            changed: [{
                                title: "children",
                                from: from ? todo.key : null,
                                to: from ? null : todo.key,
                            }]
                        }
                    });
                    if (todo.groupId) {
                        event.groupId = todo.groupId;
                    }
                    await db.save(tableEvent, event);
                    //not need to push. caz client knows parent changes from registedTodo.
                }
                if (beforeParentKey) {
                    await registParentChangeEvent(todoBeforeSave, true);
                }
                if (registedTodoParentKey) {
                    await registParentChangeEvent(registedTodo, false);
                }
            }
            const beforeChildlen = todoBeforeSave && todoBeforeSave.children || [];
            const registedChildlen = registedTodo && registedTodo.children || [];
            const registChildChangeEvent = async (todo, from) => {
                const event = new Event({
                    tenant: requestTenant, todo: todo, owner: requestAccount, type: EventType.TODO_UPDATE, summary:
                    {
                        type: EventType.TODO_UPDATE,
                        changed: [{
                            title: "parent",
                            from: from ? registedTodo.key : null,
                            to: from ? null : registedTodo.key,
                        }]
                    }
                });
                if (todo.groupId) {
                    event.groupId = todo.groupId;
                }
                await db.save(tableEvent, event);
                // must push to client. caz deleted childlen cant know from client.
                const pushTodo = await queryTodoWithPersonnelAndGroup(requestTenant, todo.id);
                if (pushTodo.group) {
                    pushTodo.lastEvent = event.summary;
                    pushTodo.isSubEvent = true;
                    ChatSender.sendToGroup(ChatSender.type.UPDATE_TODO, pushTodo);
                }
            }
            await Promise.all(beforeChildlen.filter(b => !registedChildlen.some(r => r.id == b.id)).map(async t => {
                return await registChildChangeEvent(t, true);
            }));
            await Promise.all(registedChildlen.filter(r => !beforeChildlen.some(b => r.id == b.id)).map(async t => {
                return await registChildChangeEvent(t, false);
            }));
            //end joined todo event
        })
    }
    const calcNextGroupMaxTodoOrder = async (groupId) => {
        const groupMax = await db.createQueryBuilder(tableTodoOrder)
            .select(`MAX("${tableTodoOrder}"."orderKey")`, "max")
            .where(`${tableTodoOrder}.type = :type and "groupId" = :groupId `, { type: TodoOrderType.GROUP, groupId })
            .getRawOne();
        if (!groupMax || !groupMax.max || replaceDoubleQuart(groupMax.max) == "NaN") {
            return ORDERDEFAULT.toPrecision(1000);
        } else {
            return new BigNumber(replaceDoubleQuart(groupMax.max)).plus(ORDERINCREMENTS).toPrecision(1000)
        }
    }
    const selectSelfMax = async (accountId) => {
        return await db.createQueryBuilder(tableTodoOrder)
            .select(`MAX("${tableTodoOrder}"."orderKey")`, "max")
            .where(`${tableTodoOrder}.type = :type and "accountId" = :accountId `, { type: TodoOrderType.SELF, accountId })
            .getRawOne();
    }
    app.post('/api/todos', (req, res) => {
        var group;
        save(req, res, async (todo) => {
            todo.id = null;
            if (todo.groupId) {
                group = await db.findById(tableGroup, todo.groupId);
                var seq = await Group.nextTodoSequenceValue(req.requestTenant, group);
                todo.key = group.key + "-" + seq;
            }
        }, async (todo, childrenTodo) => {
            if (!todo.personnels) {
                todo.personnels = [];
            }
            var p = todo.personnels.map(async p => {
                var selfTodoOrder = new TodoOrder();
                selfTodoOrder.todo = todo;
                selfTodoOrder.account = p;
                selfTodoOrder.type = TodoOrderType.SELF;
                const selfMax = await selectSelfMax(p.id)
                if (!selfMax || !selfMax.max || replaceDoubleQuart(selfMax.max) == "NaN") {
                    selfTodoOrder.orderKey = ORDERDEFAULT.toPrecision(1000);;
                } else {
                    selfTodoOrder.orderKey = new BigNumber(replaceDoubleQuart(selfMax.max)).plus(ORDERINCREMENTS).toPrecision(1000)
                    if (!selfTodoOrder.orderKey) {
                        throw "order " + selfTodoOrder.id + " is not calcuable " + selfTodoOrder.orderKey + " : selfmax = " + selfMax.max
                    }
                }
                return db.save(tableTodoOrder, selfTodoOrder);
            })
            const personnelOrders = await Promise.all(p);
            if (childrenTodo) {
                p = childrenTodo.map(async (t, index) => {
                    if (!t.order) {
                        t.order = [];
                    }
                    var selfTodoOrder = new TodoOrder();
                    selfTodoOrder.todo = t;
                    selfTodoOrder.account = t.personnels[0];
                    selfTodoOrder.type = TodoOrderType.SELF;
                    const selfPerOrderMax = await selectSelfMax(selfTodoOrder.account.id)
                    if (!selfPerOrderMax || !selfPerOrderMax.max || replaceDoubleQuart(selfPerOrderMax.max) == "NaN") {
                        selfTodoOrder.orderKey = ORDERDEFAULT.toPrecision(1000);;
                    } else {
                        selfTodoOrder.orderKey = new BigNumber(replaceDoubleQuart(selfPerOrderMax.max)).plus(ORDERINCREMENTS).toPrecision(1000)
                    }
                    if (!selfTodoOrder.orderKey) {
                        throw "order " + selfTodoOrder.id + " is not calcuable " + selfTodoOrder.orderKey + " : selfmax = " + selfMax.max
                    }
                    t.order.push(selfTodoOrder)
                    if (todo.groupId) {
                        let groupTodoOrder = new TodoOrder();
                        groupTodoOrder.todo = t;
                        groupTodoOrder.group = group;
                        groupTodoOrder.type = TodoOrderType.GROUP;
                        groupTodoOrder.orderKey = await calcNextGroupMaxTodoOrder(todo.groupId);
                        t.order.push(groupTodoOrder)
                        await db.save(tableTodoOrder, groupTodoOrder);
                    }
                    return await db.save(tableTodoOrder, selfTodoOrder);
                });
                await Promise.all(p);
                //Avoid circular reference
                childrenTodo.forEach(t => t.order.forEach(o => delete o.todo));
            }
            todo.order = [];
            var requestAccountSelfOrder = personnelOrders.find(o => o.account.id == req.requestAccount.id);
            if (requestAccountSelfOrder) {
                delete requestAccountSelfOrder.todo;
                todo.order.push(requestAccountSelfOrder)
            } else {
                var selfTodoOrder = new TodoOrder();
                selfTodoOrder.todo = todo;
                selfTodoOrder.type = TodoOrderType.SELF;
                selfTodoOrder.orderKey = ORDERDEFAULT.toPrecision(1000);
                todo.order.push(selfTodoOrder)
                const selfOrder = await db.save(tableTodoOrder, selfTodoOrder);
                delete selfOrder.todo;
            }
            if (!todo.groupId) {
                return;
            }
            var groupTodoOrder = new TodoOrder();
            groupTodoOrder.todo = todo;
            groupTodoOrder.group = await db.findById(tableGroup, todo.groupId);
            groupTodoOrder.type = TodoOrderType.GROUP;
            groupTodoOrder.orderKey = await calcNextGroupMaxTodoOrder(todo.groupId);
            todo.order.push(groupTodoOrder)
            const order = await db.save(tableTodoOrder, groupTodoOrder);
            delete order.todo;
        });
    })
    app.put('/api/todos', (req, res) => {
        save(req, res, null, async (todo) => {
            if (!todo.groupId) {
                return;
            }
            var personnelOrders;
            if (todo.personnel) {
                var p = todo.personnels.map(async p => {
                    var currentOrder = await db.createQueryBuilder(tableTodoOrder).leftJoinAndSelect(tableTodoOrder + ".account", tableAccount)
                        .where('type=:type and "todoId"=:todoId and "accountId"=:accountId', { type: TodoOrderType.SELF, todoId: todo.id, accountId: p.id })
                        .getOne();
                    if (currentOrder) {
                        return new Promise((res) => res(currentOrder));//do Nothing
                    }
                    var selfTodoOrder = new TodoOrder();
                    selfTodoOrder.todo = todo;
                    selfTodoOrder.account = p;
                    selfTodoOrder.type = TodoOrderType.SELF;
                    const selfMax = await selectSelfMax(p.id)
                    if (!selfMax || !selfMax.max || replaceDoubleQuart(selfMax.max) == "NaN") {
                        selfTodoOrder.orderKey = ORDERDEFAULT.toPrecision(1000);;
                    } else {
                        selfTodoOrder.orderKey = new BigNumber(replaceDoubleQuart(selfMax.max)).plus(ORDERINCREMENTS).toPrecision(1000)
                        if (!selfTodoOrder.orderKey) {
                            throw "order " + selfTodoOrder.id + " is not calcuable " + selfTodoOrder.orderKey + " : selfmax = " + selfMax.max
                        }
                    }
                    return db.save(tableTodoOrder, selfTodoOrder);
                })
                personnelOrders = await Promise.all(p);
            }

            if (!todo.order) {
                todo.order = [];
            }
            if (personnelOrders) {
                var requestAccountsOrder = personnelOrders.find(o => o.account.id == req.requestAccount.id);
                if (requestAccountsOrder) {
                    todo.order.push(requestAccountsOrder);
                }
            }
            const currentGroupOrder = await db.findOne(null, tableTodoOrder, 'type=:type and "todoId"=:todoId and "groupId"=:groupId', { type: TodoOrderType.GROUP, todoId: todo.id, groupId: todo.groupId });
            if (currentGroupOrder) {
                delete currentGroupOrder.todo;
                todo.order.push(currentGroupOrder);
                return;
            }
            //restore when group id is lacking
            var group = await db.findById(tableGroup, todo.groupId);
            var groupTodoOrder = new TodoOrder();
            groupTodoOrder.todo = todo;
            groupTodoOrder.group = group;
            groupTodoOrder.type = TodoOrderType.GROUP;
            groupTodoOrder.orderKey = await calcNextGroupMaxTodoOrder(todo.groupId);
            const registedGroupTodoOrder = await db.save(tableTodoOrder, groupTodoOrder);
            delete registedGroupTodoOrder.todo;
            todo.order.push(registedGroupTodoOrder);
        });
    })
    app.put('/api/todos/:id/putAfter', async (req, res) => {
        if (!req.params.id) {
            res.status(400).send();
            return;
        }
        var idOrKey = req.params.id;
        var putOrder = req.body;
        var objectType = putOrder.objectType;
        var type = putOrder.type;
        var typeId = putOrder.typeId;
        var beforeOrderKey = putOrder.beforeOrderKey;
        var afterOrderKey = putOrder.afterOrderKey;
        if (!type) {
            res.status(400).send();
            return;
        }
        if (!objectType || objectType == "todo") {
            var todo = Strings.isNumberFormat(idOrKey) ? await db.findById(table, idOrKey) : await db.findOne(req.requestTenant, table, "key=:id", { id: idOrKey });
            checkTodoAccessible(req, res, todo, async (todo) => {
                var srcOrder = await db.findOne(null, tableTodoOrder, `"todoId"=:id and type=:type`, { id: todo.id, type });
                var order;
                if (srcOrder) {
                    order = new TodoOrder(srcOrder);
                } else {
                    order = new TodoOrder();
                    order.todo = todo;
                    if (type == TodoOrderType.GROUP) {
                        order.group = await db.findById(tableGroup, typeId);
                    } else {
                        order.account = req.requestAccount;
                    }
                    order.type = type;
                }
                const requestTennant = req.requestTennant;// for fast response.
                const requestAccount = req.requestAccount;// for fast response.
                order.orderKey = calcOrderKey(beforeOrderKey, afterOrderKey)
                res.send(await db.save(tableTodoOrder, order));
                const event = new Event({ tenant: requestTennant, todo, owner: requestAccount, ownerName: requestAccount.name, type: EventType.TODO_CHANGE_ORDER, before: srcOrder, source: order });
                if (type == TodoOrderType.GROUP) {
                    event.groupId = typeId;
                }
                await db.save(tableEvent, event);
                if (type == TodoOrderType.GROUP) {//TODO group is empty at update
                    var pushTodo = { group: order.group || await db.findById(tableGroup, typeId), objectType, id: todo.id, key: todo.key, title: todo.title, orderKey: order.orderKey, lastEvent: event.summary };
                    ChatSender.sendToGroup(ChatSender.type.SORTED, pushTodo);
                }
            })
        } else if (objectType == "bar") {
            var sprint = await db.findOne(req.requestTenant, tableSprintBar, "id = :id", { id: idOrKey })
            if (!sprint) {
                res.status(404).send();
                return;
            }
            if (type == TodoOrderType.GROUP) {
                var group = await Group.findGroupWithMember(req.requestTenant, typeId);
                if (!group || ! await Group.isMember(req.requestTenant, group.id, req.requestAccount, group) || sprint.groupId != group.id) {
                    res.status(401).send();
                    return;
                }
            } else {
                if (sprint.accountId != typeId || typeId != req.requestAccount.id) {
                    res.status(401).send();
                    return;
                }
            }
            const srcOrderKey = sprint.orderKey;
            sprint.orderKey = calcOrderKey(beforeOrderKey, afterOrderKey);
            const event = new Event({ tenant: req.requestTennant, sprintBar: sprint, owner: req.requestAccount, ownerName: req.requestAccount.name, type: EventType.SPRINT_CHANGE_ORDER, before: srcOrderKey, source: sprint.orderKey });
            if (type == TodoOrderType.GROUP) {
                event.groupId = typeId;
            }
            await db.save(tableEvent, event);
            res.send(await db.save(tableSprintBar, sprint));
            if (type == TodoOrderType.GROUP) {
                ChatSender.sendToGroup(ChatSender.type.SORTED, { group, objectType, id: sprint.id, orderKey: sprint.orderKey, title: sprint.title, lastEvent: event.summary });
            }//not need to self. caz now moving at display.
        }
    });
    app.get('/api/todos/:id/chatMessage', async function (req, res) {
        var todo = await queryTodoWithPersonnelAndGroup(req.requestTenant, req.params.id);
        if (!todo) {
            res.status(404).send();
            return;
        }
        await checkTodoAccessible(req, res, todo, async (todo) => {
            const messages = await db.createQueryBuilder(tableChatMessage)
                .leftJoinAndSelect(`${tableChatMessage}.owner`, `${tableAccount}`)
                .where(`${tableChatMessage}."todoId" = :todoId and ${tableChatMessage}."tenantId"=:tenantId and ${tableChatMessage}."deletedAt" IS NULL`, { todoId: todo.id, tenantId: req.requestTenant.id })
                .orderBy(`${tableChatMessage}."createdAt"`, "ASC")
                .getMany();
            res.send(messages);
        });
    });
    app.post('/api/todos/:id/chatMessage', async function (req, res) {
        var content = req.body.content;
        if (!content) {
            res.status(400).send();
            return;
        }
        var todo = await queryTodoWithPersonnelAndGroup(req.requestTenant, req.params.id);
        if (!todo) {
            res.status(404).send();
            return;
        }
        await checkTodoAccessible(req, res, todo, async (todo) => {
            var chatMessage = { tenant: req.requestTenant, todo, group: todo.group, owner: req.requestAccount, ownerName: req.requestAccount.name, content };
            chatMessage = await db.save(tableChatMessage, chatMessage);
            chatMessage.hash = req.body.hash;
            ChatSender.sendToGroup(ChatSender.type.NEW, chatMessage);
            res.send();
        });
    });
    app.get('/api/todos/:id/events', async function (req, res) {
        var todo = await queryTodoWithPersonnelAndGroup(req.requestTenant, req.params.id);
        if (!todo) {
            res.status(404).send();
            return;
        }
        await checkTodoAccessible(req, res, todo, async (todo) => {
            const events = await db.getConnection().query(`SELECT e.id as id, e.type as type, e.summary as summary, e."createdAt" as "createdAt", a.name as "ownerName" FROM "${tableEvent}" e left join account a on e."ownerId" = a.id WHERE e."todoId" = $1 order by e."createdAt"`, [todo.id]);
            res.send(events);
        });
    });
}