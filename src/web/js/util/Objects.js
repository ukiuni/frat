export default {
    clone(origin) {
        return JSON.parse(JSON.stringify(origin));
    },
    overwriteWith(src, arg) {
        var deleteParams = [];
        for (var param in src) {
            if (!arg[param]) {
                deleteParams.push(param)
            }
        }
        Object.assign(src, arg);
        for (var param of deleteParams) {
            src[param] = null;
        }
    }
}