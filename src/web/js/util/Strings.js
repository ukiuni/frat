const limit = (s, l) => {
    return Math.floor(s % l);
}
export default function stringToColor(arg) {
    var seed = "";
    for (let i = 0; i < arg.length; i++) {
        var code = arg.charCodeAt(i);
        if (code) {
            seed += code;
        }
    }
    var array = [];
    //create 8 numbers
    array.push(limit(seed, 16) + 8);
    array.push(limit(seed + seed, 8));
    array.push(limit(seed * seed, 16));
    array.push(limit(seed / 100, 16) + 8);
    array.push(limit(seed / 10, 16) + 8);
    array.push(limit(seed * seed * seed, 16));
    return "#" + array.map(i => i.toString(16)).join("");
}
export function baseDomain(host) {
    return host.indexOf(".") > 0
        ? host.substring(
            host.indexOf(".") + 1,
            host.length
        )
        : host;
}
export function isMail(mail) {
    return require("../../../../app/server/util/stringUtil").isMail(mail);
}

