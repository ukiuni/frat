const JSZip = require("jszip");
import * as Group from "../model/Group";
const FILE_NAME_INFO = "info.json";
const FILE_NAME_DIR_FILES = "files";
export async function zip(account, group, todos, bars, wikis, callback) {
    // oh... no history and no comments.......　its constraint.
    var zip = new JSZip();
    var fileDir = zip.folder(FILE_NAME_DIR_FILES);

    const files = await Group.loadFiles(group.id);
    var packJSON = JSON.stringify({ exportedLocation: location.protocol + "//" + location.host, account, group, todos, bars, wikis, files, exportedAt: new Date().getTime() });
    callback({ "when": "fileIndexed", "files": files })
    for (var i = 0; i < files.length; i++) {
        const file = files[i];
        const downloadUrl = (await Group.getFileDownloadUrl(group.id, file.url)).url;
        const blob = await loadFileAsArrayBuffer(file, downloadUrl);
        const createdName = encodeURIComponent(file.url);
        fileDir.file(createdName, blob, { binary: true });
        packJSON = packJSON.split(file.url).join("zip://" + createdName);
        callback({ "when": "fileZiped", "file": file })
    }
    zip.file(FILE_NAME_INFO, packJSON);
    return await zip.generateAsync({ type: "blob" });
}
export async function readZipInfo(zipFile) {
    const zip = await JSZip.loadAsync(zipFile);
    var infoJSON = await zip.files[FILE_NAME_INFO].async('text');
    return JSON.parse(infoJSON);
}
export async function readZip(zipFile, callback) {
    const zip = await JSZip.loadAsync(zipFile);
    var infoJSON = await zip.files[FILE_NAME_INFO].async('text');
    var info = JSON.parse(infoJSON);
    var group = await callback({ "when": "beforeRestoreFile", info })
    infoJSON = infoJSON.split(info.exportedLocation + "/group/" + info.group.key).join(location.protocol + "//" + location.host + "/group/" + group.key);
    var fileDirInZip = zip.folder(FILE_NAME_DIR_FILES);
    for (var file of info.files) {
        var restoredFile = await callback({ "when": "fileFound", file, blob: await fileDirInZip.files["files/" + file.url.substring("zip://".length)].async('blob') });
        infoJSON = infoJSON.split(file.url).join(restoredFile.url);
    }
    await callback({ "when": "complete", info: JSON.parse(infoJSON) });
}
export async function loadFileAsArrayBuffer(file, url) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    return new Promise((resolve, reject) => {
        xhr.onload = function () {
            var arrayBuffer = this.response;
            var blob = new Blob([arrayBuffer], { type: file.type });
            resolve(blob);
        };
        xhr.onerror = function () {
            reject(xhr);
        }
        xhr.send();
    })
}
export function download(filename, blob) {
    const a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.download = filename;

    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}
export function requestSelectFile(accept) {
    return new Promise(resolve => {
        const input = document.createElement('input');
        input.type = 'file';
        if (accept) {
            input.accept = accept;
        }
        input.onchange = event => {
            //            document.body.removeChild(input);
            resolve(event.target.files);
        };
        input.click();
    });
};