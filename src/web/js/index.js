import 'materialize-css/dist/js/materialize.min.js';
import 'materialize-css/dist/css/materialize.min.css';
import "regenerator-runtime/runtime";
import Vue from 'vue'
import App from '../components/App'
import VueRouter from 'vue-router'
import router from './router'
import VueI18n from 'vue-i18n';
import moment from "moment"
import 'moment/locale/ja';
import * as Chat from "../js/model/Chat";
import * as Account from "../js/model/Account";
var sanitizeHtml = require('sanitize-html');

if (!window.toastListener) {
    window.toastListener = (event, message) => {
        var toast;
        if ("UPDATE_TODO" == event || "UPDATE_TODO_PROGRESS" == event) {
            if (message.isSubEvent) {
                return;
            }
            if (message.lastEvent && message.lastEvent.changed && message.lastEvent.changed.some(c => c.title == "progress" && c.to == 100)) {
                toast = appVue.$t("toast.todoCompeted");
            } else {
                toast = appVue.$t("toast.todoUpdated");
            }
        } else if ("SORTED" == event) {
            if (message.objectType == "bar") {
                toast = appVue.$t("sprints.orderChange." + message.lastEvent.changed);
            } else {
                toast = appVue.$t("todo.orderChange." + message.lastEvent.changed);
            }
        } else if ("NEW_MESSAGE" == event) {
            if (window.chattingTodoId == message.todo.id) {
                return;
            }
            var content = sanitizeHtml(message.content)
            toast = `${appVue.$t("todo.comment")}:${content.substring(0, 20)}${content.length > 20 ? '...' : ``}`;
            message.key = message.todo.key;
            message.title = message.todo.title;
        } else if ("DELETE_TODO" == event) {
            toast = appVue.$t("toast.todoDeleted");
        }
        const key = message.key || message.id;
        const title = (message.title ? message.title.substring(0, 10) : "") + (message.title && message.title.length > 10 ? "..." : "");
        const url = location.protocol + "//" + location.host + "/todo/" + key;
        appVue.$toast(`${key}:${title}`, toast, () => {
            if ("DELETE_TODO" == event) {
                return;
            }
            const win = window.open(url, "_blank", 'noopener');
            win.opener = null;
            win.location = messageLink.url;
        });
        return {
            key, title, url, event: toast
        }
    }
}

Vue.use(VueRouter)

Vue.use(VueI18n)
var locale = (window.navigator.languages && window.navigator.languages[0]) ||
    window.navigator.language ||
    window.navigator.userLanguage ||
    window.navigator.browserLanguage || "en";
if (locale && locale.substr) {
    locale = locale.substr(0, 2)
}
const data = require('./message.json');
locale = data[locale] ? locale : "en";

const i18n = new VueI18n({
    locale,
    messages: data
});

Vue.filter('date', function (value, format) {
    if (!value) return ''
    return moment(new Date(value)).format(format);
})
Vue.filter('addComma', function (val) {
    if (typeof val == "number") {
        return val.toLocaleString();
    }
    try {
        var inted = parseInt(val);
        if (inted) {
            return inted.toLocaleString();
        }
    } catch (ignore) { }
    return val;
});

Vue.prototype.$event = new Vue();
Vue.prototype.$dialog = new Vue();
Vue.prototype.$socket = Chat.connect();

const appVue = new Vue({
    router,
    i18n,
    el: '#app',
    components: { App },
    template: '<app/>'
})
Vue.prototype.$initNotification = () => {
    const openingNotifications = [];
    Vue.prototype.$toast = (title, message, onclick) => {
        var n = new Notification(title, {
            body: message,
            icon: "/images/frat_icon.png"
        });
        if (onclick) {
            n.onclick = () => {
                onclick()
                n.close();
            };
        }
        setTimeout(() => {
            n.close();
            openingNotifications.splice(openingNotifications.indexOf(n), 1);
        }, 5000);
        openingNotifications.push(n);
        while (openingNotifications.length > 3) {
            openingNotifications.shift().close();
        }
    }
}
Vue.prototype.$initInternalNotification = () => {
    Vue.prototype.$toast = (title, message, onclick) => {
        const toastHTML = `<div class="targetTodo">${title}</div><div class="message">${message}</div>`;
        const instance = M.toast({ html: toastHTML });
        instance.el.onclick = onclick;
        var toastElements = document.querySelectorAll('.toast');
        if (toastElements.length > 3) {
            for (let i = 0; i < toastElements.length - 3; i++) {
                var toastInstance = M.Toast.getInstance(toastElements[i]);
                toastInstance.dismiss();
            }
        }
    }
}
(async () => {
    const account = await Account.getAccount();
    if (window.Notification.permission == "granted" && account && account.desktopNotification) {
        Vue.prototype.$initNotification();
    } else {
        Vue.prototype.$initInternalNotification();
    }
})();
