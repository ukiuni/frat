import Router from 'vue-router'

import Login from "../components/Login";
import Logout from "../components/Logout";
import Landing from "../components/Landing";

import Content from "../components/Content";
import Home from "../components/Home";
import Todo from "../components/Todo";
import ManageManual from "../components/ManageManual";
import Group from "../components/Group";
import Account from "../components/Account";
import NewTenant from "../components/NewTenant";
import Tenant from "../components/Tenant";
import Wiki from "../components/Wiki";
import SearchTodo from "../components/SearchTodo";
import File from "../components/File";
import TenantAdmin from "../components/TenantAdmin";
import Plan from "../components/Plan";

export default new Router({
    mode: 'history',
    routes: [
        { path: "/login", component: Login },
        { path: "/space/new", component: NewTenant },
        { path: "/logout", component: Logout },
        { path: "/landing", component: Landing },
        {
            path: "/", component: Content, props: true,
            children: [
                {
                    path: '/todo/new',
                    name: 'todo-new',
                    component: Todo
                },
                {
                    path: '/ticket/new',
                    name: 'ticket-new',
                    component: Todo
                },
                {
                    path: '/todo/:todoId',
                    name: 'todo',
                    component: Todo,
                    props: true
                },
                {
                    path: '/ticket/:todoId',
                    name: 'ticker',
                    component: Todo,
                    props: true
                },
                {
                    path: '/todo/:todoId/edit',
                    name: 'todo-edit',
                    component: Todo,
                    props: true
                },
                {
                    path: '/ticket/:todoId/edit',
                    name: 'ticket-edit',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/new',
                    name: 'group-new',
                    component: Group
                },
                {
                    path: '/group/:groupId/setting',
                    name: 'group-setting',
                    component: Group
                },
                {
                    path: '/group/:groupId/manage-manual',
                    name: 'manage-manual',
                    component: ManageManual
                },
                {
                    path: '/group/:groupId/todo/new',
                    name: 'group-todo-new',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/ticket/new',
                    name: 'group-todo-new',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/todo/:todoId/edit',
                    name: 'group-todo-edit',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/ticket/:todoId/edit',
                    name: 'group-todo-edit',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/todo/:todoId',
                    name: 'group-todo',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/ticket/:todoId',
                    name: 'group-todo',
                    component: Todo,
                    props: true
                },
                {
                    path: '/group/:groupId/wiki',
                    name: 'group-wiki',
                    component: Wiki,
                    props: true
                },
                {
                    path: '/group/:groupId/wiki/:wikiId',
                    name: 'group-wiki-id',
                    component: Wiki,
                    props: true
                },
                {
                    path: '/group/:groupId/searchTodo',
                    name: 'searchTodo',
                    component: SearchTodo,
                    props: true
                },
                {
                    path: '/group/:groupId',
                    name: 'group',
                    component: Home,
                    props: true
                },
                {
                    path: '/space',
                    component: Tenant
                },
                {
                    path: '/space/plan',
                    component: Plan
                },
                {
                    path: '/account',
                    name: 'account',
                    component: Account
                },
                {
                    path: '/file',
                    name: 'file',
                    component: File
                },
                {
                    path: '/tenantAdmin',
                    name: 'tenantAdmin',
                    component: TenantAdmin
                },
                {
                    path: '*',
                    name: 'home',
                    component: Home
                },
            ]
        }
    ]
})