import http from "../util/Http"

export async function loadAllManual(groupId) {
    return (await http.get("/api/manuals/" + groupId)).data;
}

export async function uploadManual(manual) {
    return (await http.post("/api/manuals", manual)).data;
}