import http from "../util/Http"

export async function loadAll(groupId, withContent) {
    return (await http.get("/api/groups/" + groupId + "/wikis" + (withContent ? "?content=true" : ""))).data;
}
export async function load(groupId, wikiId) {
    return (await http.get("/api/groups/" + groupId + "/wikis/" + wikiId)).data;
}
export async function save(groupId, wiki) {
    return (await http[wiki.id ? "put" : "post"]("/api/groups/" + groupId + "/wikis", wiki)).data;
}
export async function deleteWiki(groupId, wiki) {
    return (await http.delete("/api/groups/" + groupId + "/wikis/" + wiki.id)).data;
}