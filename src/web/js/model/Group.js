import http from "../util/Http"

export async function loadGroup(id) {
    return (await http.get("/api/groups/" + id)).data;
}
export async function loadSelfGroups() {
    return (await http.get("/api/groups/self")).data;
}
export async function uploadGroup(group) {
    return (await http[group.id ? "put" : "post"]("/api/groups", group)).data;
}
export async function uploadItemsBulk(id, info) {
    return (await http.post("/api/groups/" + id + "/bulkUpdate", info)).data;
}
export async function addAccountToGroup(id, account) {
    return (await http.post("/api/groups/" + id + "/accounts", account)).data;
}
export async function deleteAccountFromGroup(id, account) {
    return (await http.delete("/api/groups/" + id + "/accounts", account)).data;
}
export async function loadGroupsTodo(id) {
    return (await http.get("/api/groups/" + id + "/todos")).data;
}
export async function searchTodo(id, query) {
    return (await http.get("/api/groups/" + id + "/todos/search" + query)).data;
}
export async function loadSprints(id) {
    return (await http.get("/api/groups/" + id + "/sprints")).data;
}
export async function join(id, type) {
    return (await http.post("/api/groups/" + id + "/join", { type })).data;
}
export async function leave(id) {
    return (await http.post("/api/groups/" + id + "/leave")).data;
}
export async function getOpenGroup() {
    return (await http.get("/api/groups/open")).data;
}
export async function uploadFile(id, file) {
    var uploadInfo = (await http.post("/api/groups/" + id + "/uploadUrl", { fileName: file.name, fileSize: file.size, fileType: file.type })).data;
    const formData = new FormData();
    formData.append('acl', 'private');
    Object.keys(uploadInfo.fields).filter(key => key != "file").forEach(key => {
        formData.append(key, uploadInfo.fields[key]);
    });
    formData.append("file", file.blob || file);
    var uploaded = await http.post(uploadInfo.url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    uploaded.file = uploadInfo.file;
    return uploaded;
}
export async function getFileDownloadUrl(id, url) {
    return (await http.get("/api/groups/" + id + "/downloadUrl?url=" + encodeURIComponent(url))).data;
}
export async function loadFiles(id) {
    return (await http.get('/api/groups/' + id + '/files')).data;
}
export async function deleteGroup(group, key) {
    return (await http.delete('/api/groups/' + group.id + "?key=" + group.key)).data;
}