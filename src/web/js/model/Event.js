import http from "../util/Http"

export async function loadEvent(todoId) {
    return (await http.get("/api/todos/" + todoId + "/events")).data;
}
export async function loadWikiEvent(groupId, wikiId) {
    return (await http.get("/api/groups/" + groupId + "/wikis/" + wikiId + "/events")).data;
}