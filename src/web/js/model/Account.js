import http from "../util/Http"

var account;
export async function getAccount() {
    if (account) {
        return account;
    }
    if (!http.getAuthenticationKey()) {
        return null;
    }
    return account = (await http.get("/api/accounts/self")).data;
}
export async function updateAccount(account) {
    return (await http.put("/api/accounts/self", account)).data;
}
export async function refleshAccount() {
    return account = (await http.get("/api/accounts/self")).data;
}
export async function requestSendLoginKey(mail, subdomain) {
    var requestBody = { mail };
    if (subdomain) {
        requestBody.subdomain = subdomain;
    }
    return (await http.post("/api/accounts/requestSendLoginKey", requestBody)).data;
}
export async function login(mail, loginKey, subdomain) {
    const requestBody = { mail, key: loginKey };
    if (subdomain) {
        requestBody.subdomain = subdomain;
    }
    const response = await http.post("/api/accounts/login", requestBody);
    const { key: accessKey } = response.data;
    const status = response.status;
    http.setAuthenticationKey(accessKey);
    return status;
}
export async function logout() {
    await http.post("/api/accounts/logout", { mail: (await getAccount()).mail, key: http.getAuthenticationKey() });
    clearCache();
}
export async function clearCache() {
    account = null;
    http.setAuthenticationKey(null);
}

export async function loadAccountsByNameLike(name) {
    return (await http.get("/api/accounts/byName/" + encodeURIComponent(name))).data;
}
export async function uploadIcon(file) {
    var uploadInfo = (await http.post("/api/account/selfImage/uploadUrl", { fileName: file.name, fileSize: file.size, fileType: file.type })).data;
    const formData = new FormData();
    formData.append('acl', 'public-read');
    Object.keys(uploadInfo.fields).filter(key => key != "file").forEach(key => {
        formData.append(key, uploadInfo.fields[key]);
    });
    formData.append("file", file.blob);
    var uploaded = await http.post(uploadInfo.url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    uploaded.file = uploadInfo.file;
    return uploaded;
}

