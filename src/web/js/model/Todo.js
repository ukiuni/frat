import http from "../util/Http"

export async function loadAllTodo() {
    return (await http.get("/api/todos/self")).data;
}
export async function loadTodo(id) {
    return (await http.get("/api/todos/" + id)).data;
}
export async function deleteTodo(todo) {
    return (await http.delete("/api/todos/" + todo.id)).data;
}
export async function uploadTodo(todo) {
    return (await http[todo.id ? "put" : "post"]("/api/todos", todo)).data;
}
export async function putAfter({ target, beforeOrderKey, afterOrderKey, type, typeId, objectType }) {
    return (await http.put(`/api/todos/${target.id}/putAfter`, { beforeOrderKey, afterOrderKey, type, typeId, objectType })).data;
}