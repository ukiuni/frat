const config = require("./config");

const util = {
    async clear(page, selector) {
        await page.evaluate(selector => {
            document.querySelector(selector).value = "";
        }, selector);
        await page.waitForFunction(`document.querySelector('${selector}') && document.querySelector('${selector}').value == ""`)
    },
    async setText(page, selector, text, param) {
        await this.clear(page, selector);
        await page.type(selector, text, param);
    },
    async setDate(page, selector, date) {
        var dateStrings = date.split("-");
        await page.type(selector, dateStrings[0]);
        await page.keyboard.press('ArrowRight');
        await page.type(selector, dateStrings[1]);
        await page.keyboard.press('ArrowRight');
        await page.type(selector, dateStrings[2]);
    },
    async getText(page, selector) {
        await page.waitFor(selector);
        return await page.evaluate(element => element.textContent, await page.$(selector));
    },
    async textSould(page, selector, text) {
        await page.waitForFunction(`document.querySelector("${selector}") && document.querySelector("${selector}").innerText.includes("${text}")`);
    },
    async getValue(page, selector) {
        return this.getProperty(page, selector, "value");
    },
    async getProperty(page, selector, property) {
        var value = "";
        for (var i = 0; !value && i < 10; i++) {
            value = await page.evaluate(arg => {
                return document.querySelector(arg.selector)[arg.property];
            }, { selector, property });
            await this.sleep(500);
        }
        return value;
    },
    async login(page, port, param) {
        var name = `asdf${new Date().getTime()}`;
        var testData = {
            name,
            mail: `${name}@example.com`,
            code: null,
        }
        if (param) {
            Object.assign(testData, param);
        }
        if (!param || !param.withCurrentPath) {
            await page.goto(config.endpoint(port), { waitUntil: "networkidle2" });
            await page.waitFor('#toLoginButton');
            await page.click('#toLoginButton');
        } else if (page.url().endsWith("/landing")) {
            await page.waitFor('#toLoginButton');
            await page.click('#toLoginButton');
        }
        await page.waitFor("#email")
        await page.focus("#email")
        await page.type('#email', testData.mail);
        await page.click('#sendKeyButton')
        await page.goto("http://localhost:8025", { waitUntil: "networkidle2" })//TODO remote mail handling
        await page.click('.msglist-message:nth-child(1)')
        const eh = await page.$('#preview-html');
        const frame = await eh.contentFrame();
        var activateUrl = await (await frame.$('a[target="_blank"]')).getProperty('href');
        if (testData.code) {
            activateUrl = `${config.endpoint(port)}/login?mail=${mail}&code=${code}}`
        }

        await page.goto(await activateUrl.jsonValue(), { waitUntil: "networkidle2" })
        await page.waitFor("#loginButton");

        await page.click("#loginButton");

        if (!param || !param.suspectNotLogin) {
            await page.waitFor(".todo-add-button")
        }
        return testData;
    },
    async logout(page) {
        if (!await page.$(".header_dropdown_toggle")) {
            return;
        }
        await page.click(".header_dropdown_toggle");
        await page.waitFor("#logoutDropdown");
        await this.sleep(500, "i dont know why wait.");
        await page.click("#logoutDropdown");
        await page.waitFor('#toLoginButton');
    },
    async openNewTodoPageAtCurrentGroup(page) {
        await page.waitFor(`.group_button_borderd`);
        await page.hover(`.group_button_borderd`);
        await page.waitFor(`.group_button_borderd .menu_new_todo`);
        await page.click(`.group_button_borderd .menu_new_todo`);
    },
    async clickRadio(page, selectedRadioSelector) {
        await page.evaluate(
            s => (document.querySelector(s).checked = true),
            selectedRadioSelector
        )
    },
    async setParent(page, parent) {
        await this.retry(async () => {
            await this.sleep(1000);
            await page.waitFor(".edit_parent_button")
            await page.click(".edit_parent_button")
            await page.waitFor(".moveModal");
            await page.waitFor(".moveModal #searchTodoInput");
        });
        await this.sleep(500, "for srarch-todo inputtable");
        await this.setText(page, ".moveModal #searchTodoInput", parent.title, { delay: 100 });
        await page.waitFor(".searched-todo .col");

        await page.click(".searched-todo .col");
        await page.waitFor("#title:not([disabled])");

    },
    async appendsChild(page, child) {
        await page.waitFor(".child-select")
        await page.click(".child-select")
        await page.waitFor("#searchTodoInput");
        await this.setText(page, "#searchTodoInput", child.title);
        await page.waitFor(".searched-todo .col");
        await page.click(".searched-todo .col");
        await page.waitFor("#title:not([disabled])");
    },
    async createTodo({ page, todo, accountName, atHome, withParent }) {
        await util.sleep(1000, "for edit ready");
        if (!todo) {
            const unique = new Date().getTime() + " random " + Math.floor(Math.random() * 99999999);
            todo = {
                title: "title" + unique,
                content: "content" + unique,
                point: Math.floor(Math.random() * 10) + ""
            }
        }
        if (!atHome) {
            await this.openNewTodoPageAtCurrentGroup(page);
        } else {
            await page.waitFor(".todo-add-button")
            await page.click(".todo-add-button")
        }
        await page.waitFor("#title:not([disabled])");
        if (withParent) {
            await this.setParent(page, withParent)
        }
        await this.setText(page, "#title", todo.title);
        await this.setText(page, "#content", todo.content);
        await this.setText(page, "#point", todo.point);
        await util.sleep(1000);
        if (accountName) {
            await this.setText(page, "#personnels .account-input", accountName, { delay: 100 });
            await util.sleep(1000);
            await page.click("#personnels .account-appends-button")
        }
        await this.clickEnabled(page, "#saveButton")
        await page.waitFor("#editButton")

        return todo;
    },
    async clickEnabled(page, selector) {
        await page.waitFor(selector + ':not([disabled])');
        await page.click(selector);
    },
    async createGroup(page, group, additionalMembers) {
        await this.retry(async () => {
            await page.waitFor(".group_menu .group_add")
            await page.click(".group_menu .group_add")
            await page.waitFor(".moveModal .group_add_button")
        })
        await page.click(".moveModal .group_add_button")
        await page.type('#key', group.key);
        await page.waitFor('#key');
        await page.type('#name', group.name);
        if (group.secret) {
            await page.click('#secretLabel');
        }
        await page.type('#description', group.description);
        // await page.type('#iconUrl', group.iconUrl);
        if (additionalMembers) {
            await Promise.all(additionalMembers.map(async m => {
                await this.putTextDirectry(page, ".group-account-input input", m.name);
                await this.sleep(1000);
                return await page.click(".account-appends-button");
            }))
        }
        await this.sleep(500, "for button not clickable by validator")
        await page.click('#createButton')
        await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    },
    async sleep(microsec) {
        return new Promise(r => { setTimeout(r, microsec) });
    },
    async putTextDirectry(page, selector, value) {
        await page.evaluate((selector, value) => {
            document.querySelector(selector).value = value;
        }, selector, value);
        await this.sleep(1000);
        await page.type(selector, "a");
        await page.keyboard.press('Backspace');
    },
    async putDate(page, selector, y, m, d) {
        await page.type(selector, y);
        await page.keyboard.press('ArrowRight');
        await page.type(selector, m);
        await page.keyboard.press('ArrowRight');
        await page.type(selector, d);
    },
    async retry(exec) {
        var ex;
        for (let i = 0; i < 3; i++) {
            try {
                await exec();
                ex = null;
                break;
            } catch (e) {
                ex = e;
                if (i != 2) {
                    await this.sleep(1000);
                }
            }
        }
        if (ex) {
            throw ex;
        }
    }
}
module.exports = util;